from collections import OrderedDict


class Matrix():
    def __init__(self):
        self.fn = []
        self.fn.append(OrderedDict([("№","id"),("Производитель","manufacturer"),("Артикул","article"), ("Цена пог.м","price"),
                       ("Ширина рамки","frame_width"),("Изображение","image"),("Примечание","note")]))
        self.fn.append(OrderedDict([("№","id"),("Накатка на п-карт.","knurl"),("Подрамник","underframe"),("Задник","back"),
                        ("Монтаж","mount"),("Монтаж_бисер","mount_beads"),("Лак","lacquer"),("Крепеж","fixture"),
                        ("Крепеж зеркала","fixture_mirror")]))
        self.fn.append(OrderedDict([("№","id"),("Точка","point"),("Менеджер","manager"),("Принят","start"),
                        ("Выполнение","end"),("Фамилия","second_name"),("Имя","first_name"),
                        ("Отчество","patronymic"),("Телефон","phone"),("email","email"),("Тип заказа","type"),
                        ("Описание","note"),("Цена","price"),("Количество","quantity"),("Дизайн","design"),
                        ("Итого","total"),("Отправлен","sent"),("Получен","received"),("Выдан","issued")]))
        self.fn.append(OrderedDict([("№","id"),("Точка","point"),("Менеджер","manager"),("Принят","start"),
                        ("Выполнение","end"),("Фамилия","second_name"),("Имя","first_name"),
                        ("Отчество","patronymic"),("Телефон","phone"),("Тип заказа","type"),("Описание","note"),
                        ("Артикул","article"),("Цена","price"),("Размер","size"),("Периметр","perimeter"),
                        ("Площадь","area"),("Рамка","frame"),("Стекло","glass"),("Накатка","knurl"),
                        ("Подрамник","underframe"),("Задник","back"),("Монтаж","mounting"),
                        ("Паспарту","passe"),("Лак","lacquer"),("Крепеж","fixture"),("Итого","total"),("Отправлен","sent"),
                        ("Получен","received"),("Выдан","issued")]))
        self.fn.append(OrderedDict([("№","id"),("Тип заказа","type"),("Рамка","frame"),("Стекло","glass"),("Накатка","knurl"),
                        ("Подрамник","underframe"),("Задник","back"),("Монтаж","mounting"),
                        ("Паспарту","passe"),("Лак","lacquer"),("Крепеж","fixture"),("Крепеж зеркала","fixture_mirror"),
                        ("Монтаж бисера","mounting_beads")]))
        self.fn.append(OrderedDict([("№","id"),("Фамилия","second_name"),("Имя","first_name"),("Отчество","patronymic"),
                        ("Телефон","phone"),("email","email"),("Первый заказ","join_date")]))
        self.fn.append(OrderedDict([("№","id"),("Менеджер","manager"),("Пароль","password")]))
        self.fn.append(OrderedDict([("№","id"), ("Тип","type"), ("Цена","cost")]))
        self.fn.append(OrderedDict([("№","id"), ("Тип","type"), ("Цена","cost")]))
        self.fn.append(OrderedDict([("№","id"), ("Идентификатор","point"), ("Адрес","address"), ("Телефон","phone")]))
