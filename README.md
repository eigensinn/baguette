# Baguette

## OpenSource photo shop / baguette app

### Key Features

   * The application is based on the PySide2 and QWebEngine library;
   * provides opportunities to calculate an order for photo products, as well as products of any type;
   * orders are saved in the database and edited in the application;
   * can be used by the manager as a lightweight replacement of "1C" app for keeping simple bookkeeping of several outlets.
   
### Interesting features

  * order data is transferred to the html form via the ```AddToJs(QObject)``` class;
  * A json string that is passed to the html form is formed from the list ```locals()```;
  * transfer method: via ```@QtCore.Slot(result=str)```;
  * the slot name (```Send```) must begin with an uppercase letter;
  * to get the passed values ​​on the html form side, an additional call to the nested function ```AddToJs.Send(function(retVal) {let obj = JSON.parse (retVal); ... });```;
  * to get passed values ​​without calling a nested function, instead of ```@QtCore.Slot(result=str)``` you must use ```@QtCore.Property(str)```.
  
### License

[MIT License](https://bitbucket.org/eigensinn/baguette/src/master/LICENSE)

### Build with PyInstaller on Windows

    cd path\to\baguette
    path\to\baguette>venv\Scripts\activate
    (venv) path\to\baguette>venv\Scripts\pyinstaller --onefile msvc.spec

After the build, copy to the ```dist``` files:

   * from the ```venv\Lib\site-packages\PySide2\resources``` folder
   * ```venv\Lib\site-packages\PySide2\QtWebEngineProcess.exe```
   
[Download baduette.exe](https://drive.google.com/open?id=1VCZVl2T24mH_SuV9QggXjKYpZDYPlqlL)
   
### Internalization

#### Create an empty zh_TW.ts file

    cd path\to\baguette
    path\to\baguette>venv\Scripts\activate
    (venv) path\to\baguette>pylupdate5 baguette.py -ts translations\zh_TW.ts

#### Adding translation tags

    <translation>这个词</translation>

#### Converting a .ts file to .qm

    (venv) path\to\baguette>lrelease translations\zh_TW.ts
   
## Свободное приложение для фотосалона/багетной мастерской

### Основные особенности

  * приложение основано на библиотеке PySide2 и QWebEngine;
  * предоставляет возможности для расчета заказа на фотопродукцию, а также продукцию произвольного типа;
  * заказы сохраняются в дазе данных и редактируются в приложении;
  * может использоваться менеджером в качестве легковесной замены 1С для ведения простой бухгалтерии нескольких торговых точек.

### Интересные особенности

  * данные заказа передаются в html-форму через класс ```AddToJs(QObject)```;
  * json-строка, которая передается в html-форму, формируется из списка ```locals()```;
  * способ передачи: через ```@QtCore.Slot(result=str)```;
  * имя слота (```Send```) должно начинаться с большой буквы;
  * для получения переданных значений на стороне html-формы необходим дополнительный вызов вложенной функции ```AddToJs.Send(function(retVal) {let obj = JSON.parse(retVal); ... });```;
  * чтобы получить переданные значения без вызова вложенной функции, вместо ```@QtCore.Slot(result=str)``` необходимо использовать ```@QtCore.Property(str)```.
  
### Лицензия

[MIT License](https://bitbucket.org/eigensinn/baguette/src/master/LICENSE)
  
### Сборка с помощью PyInstaller под Windows

    cd path\to\baguette
    path\to\baguette>venv\Scripts\activate
    (venv) path\to\baguette>venv\Scripts\pyinstaller --onefile msvc.spec

После сборки копируйте в ```dist``` файлы:

  * из папки ```venv\Lib\site-packages\PySide2\resources```
  * ```venv\Lib\site-packages\PySide2\QtWebEngineProcess.exe```
  
[Скачать baduette.exe](https://drive.google.com/open?id=1VCZVl2T24mH_SuV9QggXjKYpZDYPlqlL)

### Интернализация

#### Создание пустого файла zh_TW.ts

    cd path\to\baguette
    path\to\baguette>venv\Scripts\activate
    (venv) path\to\baguette>pylupdate5 baguette.py -ts translations\zh_TW.ts

#### Добавление тегов translation

    <translation>这个词</translation>

#### Конвертация файла .ts в .qm

    (venv) path\to\baguette>lrelease translations\zh_TW.ts

### Сборка под Linux
