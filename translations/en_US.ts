<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Calendar</name>
    <message>
        <location filename="baguette.py" line="245"/>
        <source>Select the date of execution of the order</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ChangeLang</name>
    <message>
        <location filename="baguette.py" line="37"/>
        <source>Change Language</source>
        <translation>Change Language</translation>
    </message>
</context>
<context>
    <name>DbAuth</name>
    <message>
        <location filename="baguette.py" line="264"/>
        <source>Database connection</source>
        <translation>Database connection</translation>
    </message>
    <message>
        <location filename="baguette.py" line="269"/>
        <source>Database name</source>
        <translation>Database name</translation>
    </message>
    <message>
        <location filename="baguette.py" line="276"/>
        <source>Manager</source>
        <translation>Manager</translation>
    </message>
    <message>
        <location filename="baguette.py" line="280"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="baguette.py" line="287"/>
        <source>Save password</source>
        <translation>Save password</translation>
    </message>
    <message>
        <location filename="baguette.py" line="289"/>
        <source>Auto connect</source>
        <translation>Auto connect</translation>
    </message>
    <message>
        <location filename="baguette.py" line="296"/>
        <source>Connect</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="baguette.py" line="307"/>
        <source>Select database</source>
        <translation>Select database</translation>
    </message>
    <message>
        <location filename="baguette.py" line="307"/>
        <source>Database files</source>
        <translation>Database files</translation>
    </message>
    <message>
        <location filename="baguette.py" line="399"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="baguette.py" line="399"/>
        <source>Not all fields are filled</source>
        <translation>Not all fields are filled</translation>
    </message>
    <message>
        <location filename="baguette.py" line="431"/>
        <source>Authorisation Error</source>
        <translation>Authorisation Error</translation>
    </message>
    <message>
        <location filename="baguette.py" line="402"/>
        <source>Invalid database name</source>
        <translation>Invalid database name</translation>
    </message>
    <message>
        <location filename="baguette.py" line="416"/>
        <source>No manager in the database</source>
        <translation>No manager in the database</translation>
    </message>
    <message>
        <location filename="baguette.py" line="431"/>
        <source>Wrong password</source>
        <translation>Wrong password</translation>
    </message>
</context>
<context>
    <name>DbFilter</name>
    <message>
        <location filename="baguette.py" line="80"/>
        <source>Remove filter</source>
        <translation>Remove filter</translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="baguette.py" line="476"/>
        <source>Select</source>
        <translation>Select</translation>
    </message>
    <message>
        <location filename="baguette.py" line="481"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="baguette.py" line="485"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="baguette.py" line="495"/>
        <source>Insert row</source>
        <translation>Insert row</translation>
    </message>
    <message>
        <location filename="baguette.py" line="501"/>
        <source>Erase row</source>
        <translation>Erase row</translation>
    </message>
    <message>
        <location filename="baguette.py" line="507"/>
        <source>Delete row</source>
        <translation>Delete row</translation>
    </message>
    <message>
        <location filename="baguette.py" line="518"/>
        <source>Restore order blank no.</source>
        <translation>Restore order blank no.</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="baguette.py" line="1036"/>
        <source>Change dots to commas when exporting</source>
        <translation>Change dots to commas when exporting</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1498"/>
        <source>General form</source>
        <translation>General form</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2251"/>
        <source>Upload image</source>
        <translation>Upload image</translation>
    </message>
    <message>
        <location filename="baguette.py" line="850"/>
        <source>To calculate</source>
        <translation>To calculate</translation>
    </message>
    <message>
        <location filename="baguette.py" line="853"/>
        <source>Checkout</source>
        <translation>Checkout</translation>
    </message>
    <message>
        <location filename="baguette.py" line="857"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="baguette.py" line="873"/>
        <source>Add filter</source>
        <translation>Add filter</translation>
    </message>
    <message>
        <location filename="baguette.py" line="882"/>
        <source>To find</source>
        <translation>To find</translation>
    </message>
    <message>
        <location filename="baguette.py" line="886"/>
        <source>Sum</source>
        <translation>Sum</translation>
    </message>
    <message>
        <location filename="baguette.py" line="890"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="baguette.py" line="893"/>
        <source>Revert</source>
        <translation>Revert</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1789"/>
        <source>Erase row</source>
        <translation>Erase row</translation>
    </message>
    <message>
        <location filename="baguette.py" line="899"/>
        <source>Insert row</source>
        <translation>Insert row</translation>
    </message>
    <message>
        <location filename="baguette.py" line="902"/>
        <source>Remove from database</source>
        <translation>Remove from database</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2194"/>
        <source>CSV import</source>
        <translation>CSV import</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1002"/>
        <source>Export to CSV</source>
        <translation>Export to CSV</translation>
    </message>
    <message>
        <location filename="baguette.py" line="912"/>
        <source>Restore the blank</source>
        <translation>Restore the blank</translation>
    </message>
    <message>
        <location filename="baguette.py" line="916"/>
        <source>Select</source>
        <translation>Select</translation>
    </message>
    <message>
        <location filename="baguette.py" line="955"/>
        <source>Calculation</source>
        <translation>Calculation</translation>
    </message>
    <message>
        <location filename="baguette.py" line="956"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="baguette.py" line="968"/>
        <source>Stock</source>
        <translation>Stock</translation>
    </message>
    <message>
        <location filename="baguette.py" line="968"/>
        <source>Material prices</source>
        <translation>Material prices</translation>
    </message>
    <message>
        <location filename="baguette.py" line="969"/>
        <source>Orders</source>
        <translation>Orders</translation>
    </message>
    <message>
        <location filename="baguette.py" line="969"/>
        <source>Baguette Orders</source>
        <translation>Baguette Orders</translation>
    </message>
    <message>
        <location filename="baguette.py" line="970"/>
        <source>Types of orders</source>
        <translation>Types of orders</translation>
    </message>
    <message>
        <location filename="baguette.py" line="970"/>
        <source>Customers</source>
        <translation>Customers</translation>
    </message>
    <message>
        <location filename="baguette.py" line="971"/>
        <source>Managers</source>
        <translation>Managers</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1888"/>
        <source>Passe-partout</source>
        <translation>Passe-partout</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1863"/>
        <source>Glass</source>
        <translation>Glass</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1508"/>
        <source>Order Options</source>
        <translation>Order Options</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1505"/>
        <source>Baguette Features</source>
        <translation>Baguette Features</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1506"/>
        <source>Additional options</source>
        <translation>Additional options</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1005"/>
        <source>Print ...</source>
        <translation>Print ...</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1008"/>
        <source>Print order</source>
        <translation>Print order</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1011"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1013"/>
        <source>Quit the program</source>
        <translation>Quit the program</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1017"/>
        <source>About baguette</source>
        <translation>About baguette</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1018"/>
        <source>About the baguette app</source>
        <translation>About the baguette app</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1022"/>
        <source>About Qt</source>
        <translation>About Qt</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1029"/>
        <source>Cancel Auto Connect</source>
        <translation>Cancel Auto Connect</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1040"/>
        <source>Change language</source>
        <translation>Change language</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1045"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1053"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1058"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1082"/>
        <source>Outlets</source>
        <translation>Outlets</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1105"/>
        <source>Customer data</source>
        <translation>Customer data</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1109"/>
        <source>Last name</source>
        <translation>Last name</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1111"/>
        <source>First name</source>
        <translation>First name</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1113"/>
        <source>Patronymic</source>
        <translation>Patronymic</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1115"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1133"/>
        <source>Phone number format</source>
        <translation>Phone number format</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1201"/>
        <source>Outlet</source>
        <translation>Outlet</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1207"/>
        <source>Order type</source>
        <translation>Order type</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1150"/>
        <source>Customer email</source>
        <translation>Customer email</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1318"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1161"/>
        <source>Price</source>
        <translation>Price</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1169"/>
        <source>Quantity</source>
        <translation>Quantity</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1179"/>
        <source>Design price</source>
        <translation>Design price</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1241"/>
        <source>Date of completion</source>
        <translation>Date of completion</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1244"/>
        <source>Date format</source>
        <translation>Date format</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1213"/>
        <source>Manufacturer</source>
        <translation>Manufacturer</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1219"/>
        <source>Vendor code</source>
        <translation>Vendor code</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1225"/>
        <source>Frame width (cm)</source>
        <translation>Frame width (cm)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1231"/>
        <source>Baguette Length (cm)</source>
        <translation>Baguette Length (cm)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1236"/>
        <source>Baguette Width (cm)</source>
        <translation>Baguette Width (cm)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1252"/>
        <source>Baguette Selection</source>
        <translation>Baguette Selection</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1282"/>
        <source>Dimensions (cm)</source>
        <translation>Dimensions (cm)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1732"/>
        <source>Notification</source>
        <translation>Notification</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1472"/>
        <source>The sum is</source>
        <translation>The sum is</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1497"/>
        <source>Baguette</source>
        <translation>Baguette</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1527"/>
        <source>Auto connect canceled!</source>
        <translation>Auto connect canceled!</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1616"/>
        <source>Delete confirmation</source>
        <translation>Delete confirmation</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1616"/>
        <source>Are you sure you want to delete the position</source>
        <translation>Are you sure you want to delete the position</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1616"/>
        <source>Restore will be impossible!</source>
        <translation>Restore will be impossible!</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1690"/>
        <source>Confirm customer addition</source>
        <translation>Confirm customer addition</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1690"/>
        <source>Are you sure you want to add a customer</source>
        <translation>Are you sure you want to add a customer</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1717"/>
        <source>Email Confirmation</source>
        <translation>Email Confirmation</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1717"/>
        <source>Add email</source>
        <translation>Add email</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1717"/>
        <source>to customer table</source>
        <translation>to customer table</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1732"/>
        <source>Order is processed!</source>
        <translation>Order is processed!</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1789"/>
        <source>To clear a row, select one of its fields</source>
        <translation>To clear a row, select one of its fields</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1799"/>
        <source>Deleting user</source>
        <translation>Deleting user</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1799"/>
        <source>Superuser cannot be deleted</source>
        <translation>Superuser cannot be deleted</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1821"/>
        <source>Database message</source>
        <translation>Database message</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1853"/>
        <source>Invoice detail</source>
        <translation>Invoice detail</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1858"/>
        <source>Baguette frame</source>
        <translation>Baguette frame</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1868"/>
        <source>Foam board knurling</source>
        <translation>Foam board knurling</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1873"/>
        <source>Stretcher</source>
        <translation>Stretcher</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1878"/>
        <source>Backcloth</source>
        <translation>Backcloth</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1883"/>
        <source>Mounting</source>
        <translation>Mounting</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1892"/>
        <source>Varnishing</source>
        <translation>Varnishing</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1897"/>
        <source>Fastener</source>
        <translation>Fastener</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1902"/>
        <source>Mirror fasteners</source>
        <translation>Mirror fasteners</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2033"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2033"/>
        <source>Not all fields are filled</source>
        <translation>Not all fields are filled</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1984"/>
        <source>cmxcm</source>
        <translation>cmxcm</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1985"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1986"/>
        <source>m2</source>
        <translation>m2</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2161"/>
        <source>Select a file</source>
        <translation>Select a file</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2201"/>
        <source>CSV data files</source>
        <translation>CSV data files</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2194"/>
        <source>Something went wrong.</source>
        <translation>Something went wrong.</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2194"/>
        <source>Maybe the CSV file has the wrong number of columns or encoding.</source>
        <translation>Maybe the CSV file has the wrong number of columns or encoding.</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2201"/>
        <source>Save file</source>
        <translation>Save file</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2251"/>
        <source>Image files</source>
        <translation>Image files</translation>
    </message>
</context>
</TS>
