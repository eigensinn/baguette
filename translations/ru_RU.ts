<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Calendar</name>
    <message>
        <location filename="baguette.py" line="245"/>
        <source>Select the date of execution of the order</source>
        <translation>Выбор даты исполнения заказа</translation>
    </message>
</context>
<context>
    <name>ChangeLang</name>
    <message>
        <location filename="baguette.py" line="37"/>
        <source>Change Language</source>
        <translation>Изменить язык</translation>
    </message>
</context>
<context>
    <name>DbAuth</name>
    <message>
        <location filename="baguette.py" line="264"/>
        <source>Database connection</source>
        <translation>Подключение к базе данных</translation>
    </message>
    <message>
        <location filename="baguette.py" line="269"/>
        <source>Database name</source>
        <translation>Имя базы данных</translation>
    </message>
    <message>
        <location filename="baguette.py" line="276"/>
        <source>Manager</source>
        <translation>Менеджер</translation>
    </message>
    <message>
        <location filename="baguette.py" line="280"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="baguette.py" line="287"/>
        <source>Save password</source>
        <translation>Сохранять пароль</translation>
    </message>
    <message>
        <location filename="baguette.py" line="289"/>
        <source>Auto connect</source>
        <translation>Автоподключение</translation>
    </message>
    <message>
        <location filename="baguette.py" line="296"/>
        <source>Connect</source>
        <translation>Подключиться</translation>
    </message>
    <message>
        <location filename="baguette.py" line="307"/>
        <source>Select database</source>
        <translation>Выбрать базу данных</translation>
    </message>
    <message>
        <location filename="baguette.py" line="307"/>
        <source>Database files</source>
        <translation>Файлы баз данных</translation>
    </message>
    <message>
        <location filename="baguette.py" line="399"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="baguette.py" line="399"/>
        <source>Not all fields are filled</source>
        <translation>Не все поля заполнены</translation>
    </message>
    <message>
        <location filename="baguette.py" line="431"/>
        <source>Authorisation Error</source>
        <translation>Ошибка авторизации</translation>
    </message>
    <message>
        <location filename="baguette.py" line="402"/>
        <source>Invalid database name</source>
        <translation>Неверное имя базы данных</translation>
    </message>
    <message>
        <location filename="baguette.py" line="416"/>
        <source>No manager in the database</source>
        <translation>Нет менеджера в базе</translation>
    </message>
    <message>
        <location filename="baguette.py" line="431"/>
        <source>Wrong password</source>
        <translation>Не тот пароль</translation>
    </message>
</context>
<context>
    <name>DbFilter</name>
    <message>
        <location filename="baguette.py" line="80"/>
        <source>Remove filter</source>
        <translation>Удалить фильтр</translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="baguette.py" line="476"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="baguette.py" line="481"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="baguette.py" line="485"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="baguette.py" line="495"/>
        <source>Insert row</source>
        <translation>Добавить строку</translation>
    </message>
    <message>
        <location filename="baguette.py" line="501"/>
        <source>Erase row</source>
        <translation>Стереть строку</translation>
    </message>
    <message>
        <location filename="baguette.py" line="507"/>
        <source>Delete row</source>
        <translation>Удалить строку</translation>
    </message>
    <message>
        <location filename="baguette.py" line="518"/>
        <source>Restore order blank no.</source>
        <translation>Восстановить бланк заказа №</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="baguette.py" line="1498"/>
        <source>General form</source>
        <translation>Общая форма</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2251"/>
        <source>Upload image</source>
        <translation>Загрузить изображение</translation>
    </message>
    <message>
        <location filename="baguette.py" line="850"/>
        <source>To calculate</source>
        <translation>Рассчитать</translation>
    </message>
    <message>
        <location filename="baguette.py" line="853"/>
        <source>Checkout</source>
        <translation>Оформить заказ</translation>
    </message>
    <message>
        <location filename="baguette.py" line="857"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="baguette.py" line="873"/>
        <source>Add filter</source>
        <translation>Добавить фильтр</translation>
    </message>
    <message>
        <location filename="baguette.py" line="882"/>
        <source>To find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="baguette.py" line="886"/>
        <source>Sum</source>
        <translation>Сумма</translation>
    </message>
    <message>
        <location filename="baguette.py" line="890"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="baguette.py" line="893"/>
        <source>Revert</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1789"/>
        <source>Erase row</source>
        <translation>Стереть строку</translation>
    </message>
    <message>
        <location filename="baguette.py" line="899"/>
        <source>Insert row</source>
        <translation>Добавить строку</translation>
    </message>
    <message>
        <location filename="baguette.py" line="902"/>
        <source>Remove from database</source>
        <translation>Удалить из базы</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2194"/>
        <source>CSV import</source>
        <translation>Импорт CSV</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1002"/>
        <source>Export to CSV</source>
        <translation>Экспорт в CSV</translation>
    </message>
    <message>
        <location filename="baguette.py" line="912"/>
        <source>Restore the blank</source>
        <translation>Восстановить бланк</translation>
    </message>
    <message>
        <location filename="baguette.py" line="916"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="baguette.py" line="955"/>
        <source>Calculation</source>
        <translation>Расчет</translation>
    </message>
    <message>
        <location filename="baguette.py" line="956"/>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <location filename="baguette.py" line="968"/>
        <source>Stock</source>
        <translation>Ассортимент</translation>
    </message>
    <message>
        <location filename="baguette.py" line="968"/>
        <source>Material prices</source>
        <translation>Цены на материалы</translation>
    </message>
    <message>
        <location filename="baguette.py" line="969"/>
        <source>Orders</source>
        <translation>Заказы</translation>
    </message>
    <message>
        <location filename="baguette.py" line="969"/>
        <source>Baguette Orders</source>
        <translation>Заказы на багет</translation>
    </message>
    <message>
        <location filename="baguette.py" line="970"/>
        <source>Types of orders</source>
        <translation>Типы заказов</translation>
    </message>
    <message>
        <location filename="baguette.py" line="970"/>
        <source>Customers</source>
        <translation>Клиенты</translation>
    </message>
    <message>
        <location filename="baguette.py" line="971"/>
        <source>Managers</source>
        <translation>Менеджеры</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1888"/>
        <source>Passe-partout</source>
        <translation>Паспарту</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1863"/>
        <source>Glass</source>
        <translation>Стекло</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1508"/>
        <source>Order Options</source>
        <translation>Опции заказа</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1505"/>
        <source>Baguette Features</source>
        <translation>Характеристики багета</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1506"/>
        <source>Additional options</source>
        <translation>Дополнительные опции</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1005"/>
        <source>Print ...</source>
        <translation>Печать...</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1008"/>
        <source>Print order</source>
        <translation>Распечатать заказ</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1011"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1013"/>
        <source>Quit the program</source>
        <translation>Выйти из программы</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1017"/>
        <source>About baguette</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1018"/>
        <source>About the baguette app</source>
        <translation>О приложении baguette</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1022"/>
        <source>About Qt</source>
        <translation>О библиотеке Qt</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1029"/>
        <source>Cancel Auto Connect</source>
        <translation>Отменить автоподключение</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1036"/>
        <source>Change dots to commas when exporting</source>
        <translation>Менять точки на запятые при экспорте</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1045"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1053"/>
        <source>Service</source>
        <translation>Сервис</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1058"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1082"/>
        <source>Outlets</source>
        <translation>Торговые точки</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1105"/>
        <source>Customer data</source>
        <translation>Данные клиента</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1109"/>
        <source>Last name</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1111"/>
        <source>First name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1113"/>
        <source>Patronymic</source>
        <translation>Отчество</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1115"/>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1133"/>
        <source>Phone number format</source>
        <translation>Формат номера</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1201"/>
        <source>Outlet</source>
        <translation>Торговая точка</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1207"/>
        <source>Order type</source>
        <translation>Тип заказа</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1150"/>
        <source>Customer email</source>
        <translation>email клиента</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1318"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1161"/>
        <source>Price</source>
        <translation>Цена</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1169"/>
        <source>Quantity</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1179"/>
        <source>Design price</source>
        <translation>Стоимость дизайна</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1241"/>
        <source>Date of completion</source>
        <translation>Дата выполнения</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1244"/>
        <source>Date format</source>
        <translation>Формат даты</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1213"/>
        <source>Manufacturer</source>
        <translation>Производитель</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1219"/>
        <source>Vendor code</source>
        <translation>Артикул</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1225"/>
        <source>Frame width (cm)</source>
        <translation>Ширина рамки (см)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1231"/>
        <source>Baguette Length (cm)</source>
        <translation>Длина багета (см)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1236"/>
        <source>Baguette Width (cm)</source>
        <translation>Ширина багета (см)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1252"/>
        <source>Baguette Selection</source>
        <translation>Подбор багета</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1282"/>
        <source>Dimensions (cm)</source>
        <translation>Размеры (см)</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1732"/>
        <source>Notification</source>
        <translation>Оповещение</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1472"/>
        <source>The sum is</source>
        <translation>Сумма составляет</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1497"/>
        <source>Baguette</source>
        <translation>Багет</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1527"/>
        <source>Auto connect canceled!</source>
        <translation>Автоподключение отменено!</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1616"/>
        <source>Delete confirmation</source>
        <translation>Подтверждение удаления</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1616"/>
        <source>Are you sure you want to delete the position</source>
        <translation>Вы уверены, что хотите удалить позицию</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1616"/>
        <source>Restore will be impossible!</source>
        <translation>Восстановление будет невозможно!</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1690"/>
        <source>Confirm customer addition</source>
        <translation>Подтверждение добавления клиента</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1690"/>
        <source>Are you sure you want to add a customer</source>
        <translation>Вы уверены, что хотите добавить клиента</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1717"/>
        <source>Email Confirmation</source>
        <translation>Подтверждение добавления email</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1717"/>
        <source>Add email</source>
        <translation>Добавить email</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1717"/>
        <source>to customer table</source>
        <translation>в таблицу клиента</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1732"/>
        <source>Order is processed!</source>
        <translation>Заказ оформлен!</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1789"/>
        <source>To clear a row, select one of its fields</source>
        <translation>Для очищения строки необходимо выделить одно из ее полей</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1799"/>
        <source>Deleting user</source>
        <translation>Удаление пользователя</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1799"/>
        <source>Superuser cannot be deleted</source>
        <translation>Пользователь superuser не может быть удален</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1821"/>
        <source>Database message</source>
        <translation>Сообщение базы данных</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1853"/>
        <source>Invoice detail</source>
        <translation>Детализация счета</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1858"/>
        <source>Baguette frame</source>
        <translation>Багетная рамка</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1868"/>
        <source>Foam board knurling</source>
        <translation>Накатка на пенокартон</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1873"/>
        <source>Stretcher</source>
        <translation>Подрамник</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1878"/>
        <source>Backcloth</source>
        <translation>Задник</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1883"/>
        <source>Mounting</source>
        <translation>Монтаж</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1892"/>
        <source>Varnishing</source>
        <translation>Лакировка</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1897"/>
        <source>Fastener</source>
        <translation>Крепеж</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1902"/>
        <source>Mirror fasteners</source>
        <translation>Крепеж зеркала</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2033"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2033"/>
        <source>Not all fields are filled</source>
        <translation>Не все поля заполнены</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1984"/>
        <source>cmxcm</source>
        <translation>см×см</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1985"/>
        <source>m</source>
        <translation>м</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1986"/>
        <source>m2</source>
        <translation>м²</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2161"/>
        <source>Select a file</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2201"/>
        <source>CSV data files</source>
        <translation>Файлы данных CSV</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2194"/>
        <source>Something went wrong.</source>
        <translation>Что-то пошло не так.</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2194"/>
        <source>Maybe the CSV file has the wrong number of columns or encoding.</source>
        <translation>Возможно CSV-файл имеет неверное количество столбцов или кодировку.</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2201"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="baguette.py" line="2251"/>
        <source>Image files</source>
        <translation>Файлы изображений</translation>
    </message>
    <message>
        <location filename="baguette.py" line="1040"/>
        <source>Change language</source>
        <translation>Изменить язык</translation>
    </message>
</context>
</TS>
