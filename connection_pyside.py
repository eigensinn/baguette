from PySide2 import QtSql


def create_local_connection(db_name):
    db = QtSql.QSqlDatabase.addDatabase('QSQLITE')
    db.setDatabaseName(db_name)
    if not db.open():
        return False
    return True
    