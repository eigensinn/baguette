import re
import sys
import shelve
import os
import csv
import connection_pyside
import kdata
import json
import typing
from PySide2.QtWidgets import QLabel, QTableView, QItemDelegate, QStyledItemDelegate, QWidget, QCalendarWidget, \
    QLineEdit, QComboBox, QStyle, QStyleOptionButton, QApplication, QStyleFactory, QSplashScreen, QProgressBar, \
    QDialog, QGridLayout, QMainWindow, QAction, QMessageBox, QTabWidget, QGroupBox, QTextEdit, QSizePolicy, QSplitter, \
    QFrame, QHBoxLayout, QAbstractButton, QSpacerItem, QSpinBox, QPushButton, QDialogButtonBox, QCheckBox, \
    QFileDialog, QMenu, QAbstractItemView, QRadioButton
from PySide2.QtPrintSupport import QPrinter, QPrintDialog
from PySide2.QtCore import Qt, QPoint, QEvent, QObject, QRect, Slot, Signal, QSettings, QSize, QRegExp, QUrl, QDate, \
    QTranslator
from PySide2.QtGui import QPixmap, QPainter, QIcon, QCursor, QFocusEvent, QRegExpValidator, QDoubleValidator
from PySide2.QtSql import QSqlDatabase, QSqlQuery, QSqlTableModel
from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PySide2.QtWebChannel import QWebChannel
from datetime import datetime
from pd import py_dir, cur_dir, jdbauth
from functools import reduce
from PIL import Image
from pathlib import Path
from glob import glob
if sys.platform.startswith('linux'):
    from OpenGL import GL


class ChangeLang(QDialog):
    def __init__(self, parent=None):
        super(ChangeLang, self).__init__(parent)

        self.setWindowIcon(QIcon(QPixmap(os.path.join(py_dir, "static/icon.svg"))))
        self.setWindowTitle(self.tr("Change Language"))
        self.setMinimumWidth(300)
        self.setWindowFlag(Qt.WindowStaysOnTopHint)

        layout_1 = QGridLayout(self)

        locales = glob("translations/*.qm")
        self.cb_locales = []
        for i, loc in enumerate(locales):
            if sys.platform.startswith('linux'):
                delimiter = '/'
            else:
                delimiter = '\\'
            loc_name = loc.split(delimiter)[-1].replace('.qm', '')
            self.cb_locales.append(QRadioButton(loc_name))
            self.cb_locales[i].setObjectName(loc_name)
            layout_1.addWidget(self.cb_locales[i], i, 0)
            self.cb_locales[i].clicked.connect(self.set_language)

    def show_change_lang(self):
        self.show()

    def set_language(self):
        app.removeTranslator(translator)
        if translator.load(self.sender().objectName() + '.qm', 'translations'):
            app.installTranslator(translator)
        with shelve.open(jdbauth) as sh:
            sh["lang"] = self.sender().objectName()

    def changeEvent(self, event):
        if event.type() == QEvent.LanguageChange:
            try:
                self.retranslate_ui()
            except NameError:
                pass
        super(ChangeLang, self).changeEvent(event)

    def retranslate_ui(self):
        self.setWindowTitle(QApplication.translate('ChangeLang', 'Change Language'))
        tooltips = [
            ('btn_load_image', 'Upload image'), ('btn_calculation', 'To calculate'), ('btn_checkout', 'Checkout'),
            ('btn_print', 'Print'), ('btn_add_filter', 'Add filter'), ('btn_search', 'To find'),
            ('btn_sum', 'Sum'), ('btn_submit', 'Save'), ('btn_revert', 'Revert'),
            ('btn_erase_row', 'Erase row'), ('btn_insert_row', 'Insert row'),
            ('btn_delete_row', 'Remove from database'), ('btn_csv_import', 'CSV import'),
            ('btn_csv_export', 'Export to CSV'), ('btn_restore', 'Restore the blank'), ('btn_select', 'Select')
        ]
        for name in tooltips:
            getattr(mw, name[0]).setToolTip(QApplication.translate('Main', name[1]))

        mw.tab_widget.setTabText(0, QApplication.translate('Main', "Calculation"))
        mw.tab_widget.setTabText(1, QApplication.translate('Main', "Database"))
        inner_tabs_names = ["Stock", "Material prices", "Orders", "Baguette Orders", "Types of orders", "Customers",
                            "Managers", "Passe-partout", "Glass", "Outlets"]
        for i in range(len(inner_tabs_names)):
            mw.inner_tab_widget.setTabText(i, QApplication.translate('Main', inner_tabs_names[i]))
        inner_tabs_names_2 = ["Order Options", "Baguette Features", "Additional options"]
        for i in range(len(inner_tabs_names_2)):
            mw.inner_tab_widget2.setTabText(i, QApplication.translate('Main', inner_tabs_names_2[i]))
        actions = [
            ('act_load_img', 'Upload image', 'Upload image'), ('act_csv_import', 'CSV import', 'CSV import'),
            ('act_csv_export', 'Export to CSV', 'Export to CSV'), ('act_print', 'Print ...', 'Print order'),
            ('act_quit', 'Quit', 'Quit the program'), ('act_about', 'About baguette', 'About the baguette app'),
            ('act_about_qt', 'About Qt', 'About Qt'),
            ('act_discard_auto_auth', 'Cancel Auto Connect', 'Cancel Auto Connect'),
            ('act_i18n', 'Change language', 'Change language'),
            ('act_comma_change', 'Change dots to commas when exporting', 'Change dots to commas when exporting')
        ]
        for name in actions:
            getattr(mw, name[0]).setText(QApplication.translate('Main', name[1]))
            getattr(mw, name[0]).setStatusTip(QApplication.translate('Main', name[2]))

        mw.menu_file.setTitle(QApplication.translate('Main', 'File'))
        mw.menu_service.setTitle(QApplication.translate('Main', 'Service'))
        mw.menu_help.setTitle(QApplication.translate('Main', 'Help'))

        mw.client_data.setTitle(QApplication.translate('Main', 'Customer data'))
        mw.selection.setTitle(QApplication.translate('Main', 'Baguette Selection'))
        mw.passe_options.setTitle(QApplication.translate('Main', 'Passe-partout'))
        label_names = [
            ('lbl_second_name', 'Last name'), ('lbl_first_name', 'First name'), ('lbl_patronymic', 'Patronymic'),
            ('lbl_phone', 'Phone'), ('lbl_point2', 'Outlet'), ('lbl_type2', 'Order type'),
            ('lbl_email', 'Customer email'), ('lbl_note', 'Description'), ('lbl_price', 'Price'),
            ('lbl_quantity', 'Quantity'), ('lbl_design', 'Design price'), ('lbl_end2', 'Date of completion'),
            ('lbl_point', 'Outlet'), ('lbl_type', 'Order type'), ('lbl_manuf', 'Manufacturer'),
            ('lbl_quantity', 'Quantity'), ('lbl_design', 'Design price'), ('lbl_end2', 'Date of completion'),
            ('lbl_article', 'Vendor code'), ('lbl_width', 'Frame width (cm)'), ('lbl_length', 'Baguette Length (cm)'),
            ('lbl_height', 'Baguette Width (cm)'), ('lbl_end', 'Date of completion'),
            ('lbl_passe_size', 'Dimensions (cm)'), ('lbl_descr', 'Description'), ('lbl_glass', 'Glass')
        ]
        for name in label_names:
            getattr(mw, name[0]).setText(QApplication.translate('Main', name[1]))


class DbFilter:
    def __init__(self, row):
        self.row = row
        
    def add_filter(self):
        self.arr = [PicButton(), QComboBox(), QComboBox(), QLineEdit()]
        self.arr[0].setToolTip(self.tr("Remove filter"))
        self.arr[0].setObjectName("hfilter")
        self.arr[1].addItems(list(kdata.Matrix().fn[0].keys()))
        self.arr[2].addItems(mw.signs)

        for i in range(0, 4):
            mw.gridonframe.addWidget(self.arr[i], self.row, i+8)
        
        self.arr[0].clicked.connect(lambda: self.hide_filter(self.row))
    
    def hide_filter(self, row):
        for i in self.arr:
            i.hide()
        self.arr[3].clear()


class Order2TableView(QTableView):
    def __init__(self, *args, **kwargs):
        QTableView.__init__(self, *args, **kwargs)
        self.setItemDelegateForColumn(3, StartTimeDelegate(self))
        self.setItemDelegateForColumn(4, EndTimeDelegate(self))
        for i in range(16, 19):
            self.setItemDelegateForColumn(i, CheckBoxDelegate(self))


class OrderTableView(QTableView):
    def __init__(self, *args, **kwargs):
        QTableView.__init__(self, *args, **kwargs)
        self.setItemDelegateForColumn(3, StartTimeDelegate(self))
        self.setItemDelegateForColumn(4, EndTimeDelegate(self))
        for i in range(26, 29):
            self.setItemDelegateForColumn(i, CheckBoxDelegate(self))


class TypesTableView(QTableView):
    def __init__(self, *args, **kwargs):
        QTableView.__init__(self, *args, **kwargs)
        for i in range(2, 13):
            self.setItemDelegateForColumn(i, CheckBoxDelegate(self))


class StartTimeDelegate(QItemDelegate):
    def __init__(self, parent):
        QItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        time = QLineEdit(parent)
        time.setInputMask("0000-00-00 00:00:00;_")
        return time


class EndTimeDelegate(QItemDelegate):
    def __init__(self, parent):
        QItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        time = QLineEdit(parent)
        time.setInputMask("0000-00-00;_")
        return time


class ManagersTableView(QTableView):
    def __init__(self, *args, **kwargs):
        QTableView.__init__(self, *args, **kwargs)
        self.setItemDelegateForColumn(2, LineEditDelegate(self))


class LineEditDelegate(QStyledItemDelegate):
    def __init__(self, parent):
        QStyledItemDelegate.__init__(self, parent)
        
    def displayText(self, value, locale):
        stars = re.sub(r".", '\u25CF', value)
        return stars

    def createEditor(self, parent, option, index):
        password = QLineEdit(parent)
        password.setEchoMode(QLineEdit.PasswordEchoOnEdit)
        return password


class CheckBoxDelegate(QStyledItemDelegate):
    def __init__(self, parent):
        QStyledItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        return None

    def paint(self, painter, option, index):
        data = index.data()
        if data == 1 or data == "true":
            checked = True
        elif data == 0 or data == "false":
            checked = False
        check_box_style_option = QStyleOptionButton()

        if (index.flags() & Qt.ItemIsEditable) > 0:
            check_box_style_option.state |= QStyle.State_Enabled
        else:
            check_box_style_option.state |= QStyle.State_ReadOnly

        if checked:
            check_box_style_option.state |= QStyle.State_On
        else:
            check_box_style_option.state |= QStyle.State_Off

        check_box_style_option.rect = self.get_checkbox_rect(option)
        check_box_style_option.state |= QStyle.State_Enabled
        QApplication.style().drawControl(QStyle.CE_CheckBox, check_box_style_option, painter)

    def editorEvent(self, event, model, option, index):
        if not (index.flags() & Qt.ItemIsEditable) > 0:
            return 0

        if event.type() == QEvent.MouseButtonPress:
            return 0
        if event.type() == QEvent.MouseButtonRelease or event.type() == QEvent.MouseButtonDblClick:
            if event.button() != Qt.LeftButton or not self.get_checkbox_rect(option).contains(event.pos()):
                return 0
            if event.type() == QEvent.MouseButtonDblClick:
                return 1
        elif event.type() == QEvent.KeyPress:
            if event.key() != Qt.Key_Space and event.key() != Qt.Key_Select:
                return 0
            else:
                return 0

        # Change the checkbox-state
        self.setModelData(None, model, index)
        return 1

    def setModelData(self, editor, model, index):
        new_value = not index.data()
        if new_value:
            new_value = 1
        if not new_value:
            new_value = 0
        model.setData(index, new_value, Qt.EditRole)

    def get_checkbox_rect(self, option):
        check_box_style_option = QStyleOptionButton()
        check_box_rect = QApplication.style().subElementRect(QStyle.SE_CheckBoxIndicator, check_box_style_option, None)
        check_box_point = QPoint(option.rect.x() + option.rect.width() / 2 - check_box_rect.width() / 2,
                                 option.rect.y() + option.rect.height() / 2 - check_box_rect.height() / 2)
        return QRect(check_box_point, check_box_rect.size())


class Calendar(QWidget):
    def __init__(self, parent=None):
        super(Calendar, self).__init__(parent)

        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.setWindowFlags(self.windowFlags() | Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowMaximizeButtonHint)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowMinimizeButtonHint)
        cal = QCalendarWidget(self)
        cal.setGridVisible(True)
        cal.move(20, 20)
        cal.clicked[QDate].connect(self.receive_date)
        
        self.lbl = QLabel(self)
        date = cal.selectedDate()
        self.lbl.setText(date.toString("ddd dd MMM yyyy"))
        self.lbl.move(100, 190)
        self.setWindowIcon(QIcon(QPixmap(os.path.join(py_dir, "static/icon.svg"))))
        self.setWindowTitle(self.tr('Select the date of execution of the order'))

    def init_ui(self, x, y):
        self.setGeometry(x+70, y+100, 260, 220)
        
    def receive_date(self, date):
        if mw.form.isChecked():
            line_edit = mw.le_end
        else:
            line_edit = mw.le_end2
        self.lbl.setText(date.toString("ddd dd MMM yyyy"))
        line_edit.setText(date.toString("dd.MM.yyyy"))


class DbAuth(QDialog):
    def __init__(self, parent=None):
        super(DbAuth, self).__init__(parent)
        
        self.setWindowIcon(QIcon(QPixmap(os.path.join(py_dir, "static/icon.svg"))))
        self.setWindowTitle(self.tr("Database connection"))
        
        layout_1 = QGridLayout(self)
        spacer = QSpacerItem(40, 10, QSizePolicy.Minimum, QSizePolicy.Expanding)

        db_name_label = QLabel(self.tr("Database name"))
        self.db_name = QLineEdit()
        btn_db_choice = QPushButton("...")
        btn_db_choice.setObjectName("dbchoice")
        layout_1.addWidget(db_name_label, 0, 0)
        layout_1.addWidget(self.db_name, 0, 1)
        layout_1.addWidget(btn_db_choice, 0, 2)
        lbl_manager = QLabel(self.tr("Manager"))
        self.manager = QLineEdit()
        layout_1.addWidget(lbl_manager, 1, 0)
        layout_1.addWidget(self.manager, 1, 1)
        lbl_password = QLabel(self.tr("Password"))
        self.password = QLineEdit()
        self.password.setEchoMode(QLineEdit.PasswordEchoOnEdit)
        layout_1.addWidget(lbl_password, 2, 0)
        layout_1.addWidget(self.password, 2, 1)
        
        layout_1.addItem(spacer, 3, 0)
        self.save_password_cb = QCheckBox(self.tr("Save password"))
        layout_1.addWidget(self.save_password_cb, 4, 0)
        self.cb_auto_auth = QCheckBox(self.tr("Auto connect"))
        layout_1.addWidget(self.cb_auto_auth, 4, 1)
        layout_1.addItem(spacer, 5, 0)
        
        self.shelve_fill()
        
        bd_auth_btn_box = QDialogButtonBox(Qt.Horizontal)
        btn_connect = QPushButton(self.tr("Connect"))
        btn_connect.setDefault(True)
        bd_auth_btn_box.addButton(btn_connect, QDialogButtonBox.AcceptRole)
        layout_1.addWidget(bd_auth_btn_box, 6, 0, 1, 2)
        
        btn_connect.clicked.connect(self.shelve_write)
        self.cb_auto_auth.stateChanged.connect(lambda: self.auto_auth(0))
        self.save_password_cb.stateChanged.connect(lambda: self.auto_auth(1))
        btn_db_choice.clicked.connect(self.db_choice)
        
    def db_choice(self):
        file_name, _ = QFileDialog.getOpenFileName(
            self, self.tr('Select database'), os.getcwd(), self.tr("Database files")+" (*.sqlite)")
        if file_name != "":
            self.db_name.setText(file_name)
        
    def auto_auth(self, v):
        with shelve.open(jdbauth) as s:
            if v == 0:
                if self.cb_auto_auth.isChecked():
                    s["auto"] = 1
                else:
                    s["auto"] = 0
            elif v == 1:
                if self.save_password_cb.isChecked():
                    s["savepass"] = 1
                else:
                    s["savepass"] = 0
        
    def shelve_fill(self):
        with shelve.open(jdbauth) as s:
            try:
                self.db_name.setText(s["auth"][0])
                self.manager.setText(s["auth"][1])
            except KeyError:
                pass
            try:
                if s["savepass"] == 1:
                    self.save_password_cb.setChecked(True)
                    self.password.setText(s["auth"][2])
            except KeyError:
                pass
            try:
                if s["auto"] == 1:
                    self.cb_auto_auth.setChecked(True)
            except KeyError:
                pass
        
    def shelve_read(self):
        try:
            with shelve.open(jdbauth) as s:
                a = s["auth"]
                auto = s["auto"]
        except KeyError:
            return False
        else:
            if auto == 1:
                connection = connection_pyside.create_local_connection(os.path.join(py_dir, a[0]))
                if connection:
                    query = QSqlQuery()
                    query.prepare("SELECT manager FROM managers WHERE manager = ?")
                    query.addBindValue(a[1])
                    query.exec_()
                    while query.next():
                        manager = query.value(0)
                    try:
                        manager
                    except Exception as ex:
                        template = "shelve_read: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return False
                    else:
                        query.prepare("SELECT password FROM managers WHERE manager = ?")
                        query.addBindValue(manager)
                        query.exec_()
                        while query.next():
                            password = query.value(0)
                        if password == a[2]:
                            return True
                        else:
                            return False
            else:
                return False

    def mw_init(self):
        self.close()
        global mw
        global px
        mw = Main()
        mw.show()
        mw.combo_fill()
        px = Pix()
        px.load_canvas()
        self.cldr = Calendar()
        self.matrix = kdata.Matrix()

    def shelve_write(self):
        db_name = self.db_name.text()
        db_path = os.path.join(py_dir, db_name)
        manager_text = self.manager.text()
        pwd_text = self.password.text()
        if db_name is None or manager_text is None or pwd_text is None:
            QMessageBox.warning(self, self.tr("Error"), self.tr("Not all fields are filled"))
        else:
            if not connection_pyside.create_local_connection(db_path):
                QMessageBox.warning(self, self.tr("Authorisation Error"), self.tr("Invalid database name"))
            else:
                query = QSqlQuery()
                query.prepare("SELECT manager FROM managers WHERE manager = ?")
                query.addBindValue(manager_text)
                query.exec_()
                while query.next():
                    manager = query.value(0)
                try:
                    manager
                except Exception as ex:
                    template = "shelve_write: {0} occurred. Arguments:\n{1!r}"
                    message = template.format(type(ex).__name__, ex.args)
                    print(message)
                    QMessageBox.warning(self, self.tr("Authorisation Error"), self.tr("No manager in the database"))
                    db = QSqlDatabase.database()
                    db.close()
                else:
                    query.prepare("SELECT password FROM managers WHERE manager = ?")
                    query.addBindValue(manager)
                    query.exec_()
                    while query.next():
                        password = query.value(0)
                    if password == pwd_text:
                        with shelve.open(jdbauth) as s:
                            s["auth"] = [db_name, manager, password]
                        self.mw_init()
                    else:
                        QMessageBox.warning(self, self.tr("Authorisation Error"), self.tr("Wrong password"))
                        db = QSqlDatabase.database()
                        db.close()
        

class PicButton(QAbstractButton):
    def __init__(self, parent=None):
        super(PicButton, self).__init__(parent)
        self.setMouseTracking(True)
        
    def paintEvent(self, event):
        painter = QPainter(self)
        names = ["calculator", "checkout", "print", "save", "revert", "erase", "insert", "delete", "import",
                 "export", "search", "general", "baguette", "imageload", "sum", "filter", "hfilter", "restore",
                 "select"]
        for i in names:
            if self.isEnabled():
                if self.objectName() == i:
                    painter.drawPixmap(event.rect(), QPixmap(os.path.join(py_dir, "static/enabled/"+i+".png")))
                if self.objectName() == i+"_hover":
                    painter.drawPixmap(event.rect(), QPixmap(os.path.join(py_dir, "static/hover/"+i+".png")))
            else:
                if self.objectName() == i:
                    painter.drawPixmap(event.rect(), QPixmap(os.path.join(py_dir, "static/disabled/"+i+".png")))

    def enterEvent(self,event):
        if self.isEnabled():
            n = self.objectName()
            self.setObjectName(n+"_hover")
            self.setCursor(Qt.PointingHandCursor)

    def leaveEvent(self,event):
        n = self.objectName()
        self.setObjectName(n.replace("_hover", ""))


class Filter(QObject):
    def eventFilter(self, widget, event):
        if event.type() == QEvent.ContextMenu:
            mw.contextmenu = QMenu(mw)
            dic = mw.model_definition()
            sm = dic["sm"]
            if sm == mw.selection_models[0] and mw.form.isChecked() or sm == mw.selection_models[5]:
                act_select = QAction(QIcon(os.path.join(py_dir, 'static/menu/select.png')), self.tr('Select'), mw)
                act_select.triggered.connect(mw.Select)
                act_select.setStatusTip(self.tr("Select"))
                mw.contextmenu.addAction(act_select)
                mw.contextmenu.addSeparator()
            act_revert = QAction(QIcon(os.path.join(py_dir, 'static/menu/revert.png')), self.tr('Cancel'), mw)
            act_revert.triggered.connect(mw.revert)
            act_revert.setStatusTip(self.tr("Cancel"))
            mw.contextmenu.addAction(act_revert)
            act_submit = QAction(QIcon(os.path.join(py_dir, 'static/menu/save.png')), self.tr('Save'), mw)
            act_submit.triggered.connect(mw.submit)
            act_submit.setStatusTip(self.tr("Save"))
            mw.contextmenu.addAction(act_submit)
            mw.contextmenu.addSeparator()
            if not dbauth.manager.text() == "superuser" and sm == mw.selection_models[6]:
                pass
            else:
                if not sm == mw.selection_models[1]:
                    act_insert = QAction(QIcon(os.path.join(py_dir, 'static/menu/insert.png')), self.tr('Insert row'),
                                         mw)
                    act_insert.triggered.connect(mw.insert_row)
                    act_insert.setStatusTip(self.tr('Insert row'))
                    mw.contextmenu.addAction(act_insert)
                value = sm.model().index(sm.currentIndex().row(), 0).data()
                act_erase = QAction(QIcon(os.path.join(py_dir, 'static/menu/erase.png')),
                                    self.tr('Erase row')+' {0}'.format(value), mw)
                act_erase.triggered.connect(mw.erase_row)
                act_erase.setStatusTip(self.tr('Erase row')+' {0}'.format(value))
                mw.contextmenu.addAction(act_erase)
                if not sm == mw.selection_models[1]:
                    act_delete = QAction(QIcon(os.path.join(py_dir, 'static/menu/delete.png')),
                                         self.tr('Delete row')+' {0}'.format(value), mw)
                    act_delete.triggered.connect(mw.delete_row)
                    act_delete.setStatusTip(self.tr('Delete row')+' {0}'.format(value))
                    mw.contextmenu.addAction(act_delete)
            if mw.form.isChecked():
                n = mw.selection_models[3]
            else:
                n = mw.selection_models[2]
            if sm == n:
                value2 = sm.model().index(sm.currentIndex().row(), 1).data()
                act_restore = QAction(QIcon(os.path.join(py_dir, 'static/menu/restore.png')),
                                      self.tr('Restore order blank no.')+' {0}{1}'.format(value2, value), mw)
                act_restore.triggered.connect(mw.restore)
                act_restore.setStatusTip(self.tr('Restore order blank no.')+' {0}{1}'.format(value2, value))
                mw.contextmenu.addAction(act_restore)
            mw.contextmenu.popup(QCursor.pos())
        if event.type() == QEvent.FocusIn:
            try:
                wt = widget.text()
                if wt == "←" or wt == "→" or wt == "↑" or wt == "↓":
                    widget.clear()
                    widget.setStyleSheet("color: #000;")
            except AttributeError:
                pass
            return False
        elif event.type() == QEvent.FocusOut:
            if widget.objectName() == "passe_left" and widget.text() == "":
                widget.setStyleSheet("color: #888;")
                widget.setText("←")
            elif widget.objectName() == "passe_right" and widget.text() == "":
                widget.setStyleSheet("color: #888;")
                widget.setText("→")
            elif widget.objectName() == "passe_top" and widget.text() == "":
                widget.setStyleSheet("color: #888;")
                widget.setText("↑")
            elif widget.objectName() == "passe_bottom" and widget.text() == "":
                widget.setStyleSheet("color: #888;")
                widget.setText("↓")
            return False
        else:
            return False


class Pix:
    def __init__(self, px_name=os.path.join(py_dir, "static/canvas.png")):
        self.px_name = px_name
        try:
            s = shelve.open(jdbauth)
            self.px_name = s["pix"]
            s.close()
            file_path = Path(self.px_name)
            if not file_path.is_file():
                self.px_name = os.path.join(py_dir, "static/canvas.png")
        except Exception as ex:
            template = "Pix: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            self.px_name == os.path.join(py_dir, "static/canvas.png")

        im = Image.open(self.px_name)
        im_width, im_height = im.size
        if im_width > im_height:
            self.pixmap = QPixmap(self.px_name).scaledToWidth(0.8 * mw.canvas_label.width())
        elif im_height > im_width:
            self.pixmap = QPixmap(self.px_name).scaledToHeight(0.8 * mw.canvas_label.width())

    def draw_image(self):
        article = mw.combo_article.currentText()
        query = QSqlQuery()
        query.prepare("SELECT image FROM stock WHERE article = ?")
        query.addBindValue(article)
        query.exec_()
        while query.next():
            image = query.value(0)
        try:
            return image
        except Exception as ex:
            template = "draw_image: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            return None
        
    def load_canvas(self):
        mw.canvas_label.setPixmap(self.pixmap)
        self.load_baguette()
        
    def load_baguette(self):
        px_width = self.pixmap.size().width()
        px_height = self.pixmap.size().height()
        try:
            mw.image_label.setPixmap(
                QPixmap(os.path.join(py_dir, "static/") +
                        self.draw_image()).scaled(1.25*px_width,
                                                  1.25*px_height, Qt.IgnoreAspectRatio, Qt.FastTransformation))
        except Exception as ex:
            template = "load_baguette: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)


class Detail:
    def __init__(self, order_type, length, height, width, p, s, price, passe_type, glass_type):
        self.length = length
        self.height = height
        self.width = width
        self.p = p
        self.s = s
        self.price = price
        self.mw = Main()
        query = QSqlQuery()
        query.exec_("SELECT * FROM materials_cost")
        while query.next():
            self.c_knurl = query.value(1)
            self.c_underframe = query.value(2)
            self.c_back = query.value(3)
            self.c_mount = query.value(4)
            self.c_mount_beads = query.value(5)
            self.c_lacquer = query.value(6)
            self.c_fixture = query.value(7)
            self.c_fixture_mirror = query.value(8)
        query.prepare("SELECT * FROM types WHERE type = ?")
        query.addBindValue(order_type)
        query.exec_()
        while query.next():
            self.baguette_frame = query.value(2)
            self.glass = query.value(3)
            self.knurl = query.value(4)
            self.underframe = query.value(5)
            self.back = query.value(6)
            self.mounting = query.value(7)
            self.passe_partout = query.value(8)
            self.lacquer = query.value(9)
            self.fixture = query.value(10)
            self.fixture_mirror = query.value(11)
            self.mounting_beads = query.value(12)
        query.prepare("SELECT cost FROM passe_partout WHERE type = ?")
        query.addBindValue(passe_type)
        query.exec_()
        while query.next():
            self.c_pass = query.value(0)
        query.prepare("SELECT cost FROM glass WHERE type = ?")
        query.addBindValue(glass_type)
        query.exec_()
        while query.next():
            self.c_glass = query.value(0)

    def delta_fnc(self, parameter, value):
        if not parameter:
            variable = 0
        else:
            variable = value
        return variable
    
    def mounting_fnc(self):
        if self.mounting:
            mounting = self.c_mount
        elif self.mounting_beads:
            mounting = self.c_mount_beads
        else:
            mounting = 0
        return mounting


class TableEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(TableEditor, self).__init__(parent)
        
        self.setTable("stock")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[0].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class MaterialsEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(MaterialsEditor, self).__init__(parent)
        
        self.setTable("materials_cost")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[1].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class OrderEditor2(QSqlTableModel):
    def __init__(self, parent=None):
        super(OrderEditor2, self).__init__(parent)
        
        self.setTable("new_order2")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[2].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class OrderEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(OrderEditor, self).__init__(parent)
        
        self.setTable("new_order")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[3].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class TypesEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(TypesEditor, self).__init__(parent)
        
        self.setTable("types")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[4].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class CustomersEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(CustomersEditor, self).__init__(parent)
        
        self.setTable("customers")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[5].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class ManagersEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(ManagersEditor, self).__init__(parent)
        
        self.setTable("managers")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[6].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])
        mt = dbauth.manager.text()
        if not mt == "superuser":
            self.setFilter("manager='{0}'".format(mt))


class PasseEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(PasseEditor, self).__init__(parent)
        
        self.setTable("passe_partout")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[7].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class GlassEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(GlassEditor, self).__init__(parent)
        
        self.setTable("glass")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[8].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class PointsEditor(QSqlTableModel):
    def __init__(self, parent=None):
        super(PointsEditor, self).__init__(parent)
        
        self.setTable("points")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        labels = list(kdata.Matrix().fn[9].keys())
        for i in range(0, len(labels)):
            self.setHeaderData(i, Qt.Horizontal, labels[i])


class AddToJs(QObject):
    def __init__(self, order, manager, start, end, second_name, first_name, patronymic, total, phone, order_type, note,
                 image, article, price, size, perimeter, area, detail, email, quantity, design):
        super(AddToJs, self).__init__()
        self.order = order
        self.manager = manager
        self.start = start
        self.end = end
        self.second_name = second_name
        self.first_name = first_name
        self.patronymic = patronymic
        self.phone = phone
        self.email = email
        self.order_type = order_type
        self.note = note
        self.price = price
        self.quantity = quantity
        self.design = design
        self.total = total
        self.image = image
        self.article = article
        self.size = size
        self.perimeter = perimeter
        self.area = area
        self.detail = detail
        self.send = locals()
        if 'self' in self.send:
            del self.send['self']
        if '__class__' in self.send:
            del self.send['__class__']

    @Slot(result=str)
    def Send(self):
        return json.dumps(self.send)


class Main(QMainWindow):
    focusedIn = Signal(QFocusEvent)

    def __init__(self):
        QMainWindow.__init__(self)
        
        self.filters = []

        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)
        central_layout = QGridLayout(central_widget)
        central_layout.setRowStretch(1, 1)
        self.setWindowIcon(QIcon(QPixmap(os.path.join(py_dir, "static/icon.svg"))))
        
        # -------------------------Управление-----------------------------------
        self.form = PicButton()
        self.form.setObjectName("general")
        self.form.setToolTip(self.tr("General form"))
        self.form.setCheckable(True)
        self.btn_load_image = PicButton()
        self.btn_load_image.setObjectName("imageload")
        self.btn_load_image.setToolTip(self.tr("Upload image"))
        self.btn_calculation = PicButton()
        self.btn_calculation.setToolTip(self.tr("To calculate"))
        self.btn_calculation.setObjectName("calculator")
        self.btn_checkout = PicButton()
        self.btn_checkout.setToolTip(self.tr("Checkout"))
        self.btn_checkout.setObjectName("checkout")
        self.btn_checkout.setEnabled(False)
        self.btn_print = PicButton()
        self.btn_print.setToolTip(self.tr("Print"))
        self.btn_print.setObjectName("print")
        self.btn_print.setEnabled(False)

        fr_button_box = QFrame(self)
        self.gridonframe = QGridLayout(fr_button_box)
        self.gridonframe.addWidget(self.form, 0, 0)
        self.gridonframe.addWidget(self.btn_load_image, 0, 1)
        self.gridonframe.addWidget(self.btn_calculation, 0, 2)
        self.gridonframe.addWidget(self.btn_checkout, 0, 3)
        self.gridonframe.addWidget(self.btn_print, 0, 4)
        
        self.le_line_manager = QLineEdit()
        self.le_line_manager.setReadOnly(True)
        
        self.btn_add_filter = PicButton()
        self.btn_add_filter.setToolTip(self.tr("Add filter"))
        self.btn_add_filter.setObjectName("filter")
        self.combo_column = QComboBox()
        self.combo_column.addItems(list(kdata.Matrix().fn[0].keys()))
        self.combo_sign = QComboBox()
        self.signs = ["=", ">", "<", ">=", "<="]
        self.combo_sign.addItems(self.signs)
        self.le_search = QLineEdit()
        self.btn_search = PicButton()
        self.btn_search.setToolTip(self.tr("To find"))
        self.btn_search.setObjectName("search")
        
        self.btn_sum = PicButton()
        self.btn_sum.setToolTip(self.tr("Sum"))
        self.btn_sum.setObjectName("sum")
        self.btn_sum.setEnabled(False)
        self.btn_submit = PicButton()
        self.btn_submit.setToolTip(self.tr("Save"))
        self.btn_submit.setObjectName("save")
        self.btn_revert = PicButton()
        self.btn_revert.setToolTip(self.tr("Revert"))
        self.btn_revert.setObjectName("revert")
        self.btn_erase_row = PicButton()
        self.btn_erase_row.setToolTip(self.tr("Erase row"))
        self.btn_erase_row.setObjectName("erase")
        self.btn_insert_row = PicButton()
        self.btn_insert_row.setToolTip(self.tr("Insert row"))
        self.btn_insert_row.setObjectName("insert")
        self.btn_delete_row = PicButton()
        self.btn_delete_row.setToolTip(self.tr("Remove from database"))
        self.btn_delete_row.setObjectName("delete")
        self.btn_delete_row.setEnabled(False)
        self.btn_csv_import = PicButton()
        self.btn_csv_import.setToolTip(self.tr("CSV import"))
        self.btn_csv_import.setObjectName("import")
        self.btn_csv_export = PicButton()
        self.btn_csv_export.setToolTip(self.tr("Export to CSV"))
        self.btn_csv_export.setObjectName("export")
        self.btn_restore = PicButton()
        self.btn_restore.setToolTip(self.tr("Restore the blank"))
        self.btn_restore.setObjectName("restore")
        self.btn_restore.setEnabled(False)
        self.btn_select = PicButton()
        self.btn_select.setToolTip(self.tr("Select"))
        self.btn_select.setObjectName("select")
        self.btn_select.setEnabled(False)

        vert_spacer = QSpacerItem(24, 24, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.gridonframe.addItem(vert_spacer, 0, 5)
        self.gridonframe.setColumnStretch(5, 1)
        
        self.gridonframe.addWidget(self.le_line_manager, 0, 6)
        self.gridonframe.addItem(vert_spacer, 0, 7)
        self.gridonframe.addWidget(self.btn_add_filter, 0, 8)
        self.gridonframe.addWidget(self.combo_column, 0, 9)
        self.gridonframe.addWidget(self.combo_sign, 0, 10)
        self.gridonframe.addWidget(self.le_search, 0, 11)
        self.gridonframe.addWidget(self.btn_search, 0, 12)
        self.gridonframe.addWidget(self.btn_sum, 0, 13)
        self.gridonframe.addItem(vert_spacer, 0, 14)
        self.gridonframe.addWidget(self.btn_submit, 0, 15)
        self.gridonframe.addWidget(self.btn_revert, 0, 16)
        self.gridonframe.addWidget(self.btn_insert_row, 0, 17)
        self.gridonframe.addWidget(self.btn_erase_row, 0, 18)
        self.gridonframe.addWidget(self.btn_delete_row, 0, 19)
        self.gridonframe.addWidget(self.btn_csv_import, 0, 20)
        self.gridonframe.addWidget(self.btn_csv_export, 0, 21)
        self.gridonframe.addWidget(self.btn_restore, 0, 22)
        self.gridonframe.addWidget(self.btn_select, 0, 23)

        central_layout.addWidget(fr_button_box, 0, 0)
        # -------------------------------------------------------------------------
        # ----------------------------Вкладки--------------------------------------
        self.tab_widget = QTabWidget()
        central_layout.addWidget(self.tab_widget, 1, 0, 1, 2)
        
        size_policy_e = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        size_policy_m = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.tab_widget.setSizePolicy(size_policy_e)
        self.tab_widget.setMinimumSize(QSize(720, 400))
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab_widget.addTab(self.tab2, self.tr("Calculation"))
        self.tab_widget.addTab(self.tab1, self.tr("Database"))
        self.tab_widget.move(0, 20)
        
        self.inner_tab_widget = QTabWidget()
        self.inner_tab_widget.setSizePolicy(size_policy_e)
        self.inner_tab_widget.setMinimumSize(QSize(500, 400))
        
        self.views = [QTableView(), QTableView(), Order2TableView(), OrderTableView(), TypesTableView(),
                      QTableView(), ManagersTableView(), QTableView(), QTableView()]
        lsv = len(self.views)
        
        self.tabs = [QWidget() for i in range(lsv)]
        tabs = [(self.tabs[0], self.tr("Stock")), (self.tabs[1], self.tr("Material prices")),
                (self.tabs[2], self.tr("Orders")), (self.tabs[3], self.tr("Baguette Orders")),
                (self.tabs[4], self.tr("Types of orders")), (self.tabs[5], self.tr("Customers")),
                (self.tabs[6], self.tr("Managers")), (self.tabs[7], self.tr("Passe-partout")),
                (self.tabs[8], self.tr("Glass"))]
        for i, j in tabs:
            self.inner_tab_widget.addTab(i, j)
            
        self.inner_tab_widget2 = QTabWidget()
        self.inner_tab_widget2.setSizePolicy(size_policy_m)
        self.tabs2 = [QWidget() for i in range(3)]
        inner_tabs_2 = [(self.tabs2[0], self.tr("Order Options")), (self.tabs2[1], self.tr("Baguette Features")),
                        (self.tabs2[2], self.tr("Additional options"))]
        for i, j in inner_tabs_2:
            self.inner_tab_widget2.addTab(i, j)
            
        self.models = [TableEditor(self), MaterialsEditor(self), OrderEditor2(self), OrderEditor(self),
                       TypesEditor(self), CustomersEditor(self), ManagersEditor(self), PasseEditor(self),
                       GlassEditor(self)]
        
        # Меню File
        self.act_load_img = QAction(QIcon(os.path.join(py_dir, 'static/menu/imageload.png')), self.tr('Upload image'), self)
        self.act_load_img.setShortcut('Ctrl+L')
        self.act_load_img.setStatusTip(self.tr('Upload image'))
        self.act_load_img.triggered.connect(self.load_img)
        
        self.act_csv_import = QAction(QIcon(os.path.join(py_dir, 'static/menu/import.png')), self.tr('CSV import'), self)
        self.act_csv_import.setShortcut('Ctrl+I')
        self.act_csv_import.setStatusTip(self.tr('CSV import'))
        self.act_csv_import.triggered.connect(self.csv_import)
        
        self.act_csv_export = QAction(QIcon(os.path.join(py_dir, 'static/menu/export.png')), self.tr('Export to CSV'),
                                      self)
        self.act_csv_export.setShortcut('Ctrl+X')
        self.act_csv_export.setStatusTip(self.tr('Export to CSV'))
        self.act_csv_export.triggered.connect(self.csv_export)
        
        self.act_print = QAction(QIcon(os.path.join(py_dir, 'static/menu/print.png')), self.tr('Print ...'), self)
        self.act_print.setEnabled(False)
        self.act_print.setShortcut('Ctrl+P')
        self.act_print.setStatusTip(self.tr('Print order'))
        self.act_print.triggered.connect(self.printer)
        
        self.act_quit = QAction(QIcon(os.path.join(py_dir, 'static/menu/exit.png')), self.tr('Quit'), self)
        self.act_quit.setShortcut('Ctrl+Q')
        self.act_quit.setStatusTip(self.tr('Quit the program'))
        self.act_quit.triggered.connect(self.close)
        
        # Меню Help
        self.act_about = QAction(QIcon(os.path.join(py_dir, 'static/icon.svg')), self.tr('About baguette'), self)
        self.act_about.setStatusTip(self.tr('About the baguette app'))
        self.act_about.triggered.connect(self.about)
        
        self.act_about_qt = QAction(QIcon(os.path.join(py_dir, 'static/menu/qt.png')), self.tr('About Qt'), self)
        self.act_about_qt.setStatusTip(self.tr('About Qt'))
        self.act_about_qt.triggered.connect(QApplication.aboutQt)
        
        # Меню Сервис
        self.act_discard_auto_auth = QAction(
            QIcon(os.path.join(py_dir, 'static/menu/discard.png')), self.tr('Cancel Auto Connect'), self)
        self.act_discard_auto_auth.setShortcut('Ctrl+A')
        self.act_discard_auto_auth.setStatusTip(self.tr('Cancel Auto Connect'))
        self.act_discard_auto_auth.triggered.connect(self.discard_auto_auth)
        self.act_discard_auto_auth.setEnabled(False)
        
        self.act_comma_change = QAction(QIcon(os.path.join(py_dir, 'static/menu/comma.png')),
                                        self.tr('Change dots to commas when exporting'), self, checkable=True)
        self.act_comma_change.setShortcut('Ctrl+B')
        self.act_comma_change.setStatusTip(self.tr('Change dots to commas when exporting'))
        self.act_comma_change.triggered.connect(self.comma_change)
        self.act_i18n = QAction(QIcon(os.path.join(py_dir, 'static/menu/language.svg')),
                                self.tr('Change language'), self)
        self.act_i18n.setStatusTip(self.tr('Change language'))
        self.act_i18n.triggered.connect(chl.show_change_lang)
        
        menu_bar = self.menuBar()
        self.menu_file = menu_bar.addMenu(self.tr('File'))
        self.menu_file.addAction(self.act_load_img)
        self.menu_file.addAction(self.act_csv_import)
        self.menu_file.addAction(self.act_csv_export)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.act_print)
        self.menu_file.addAction(self.act_quit)
        
        self.menu_service = menu_bar.addMenu(self.tr('Service'))
        self.menu_service.addAction(self.act_discard_auto_auth)
        self.menu_service.addAction(self.act_comma_change)
        self.menu_service.addAction(self.act_i18n)
        
        self.menu_help = menu_bar.addMenu(self.tr('Help'))
        self.menu_help.addAction(self.act_about)
        self.menu_help.addAction(self.act_about_qt)

        self.statusBar()
        
        vm = [(self.views[i], self.models[i]) for i in range(lsv)]
        for i, j in vm:
            i.setModel(j)
        self.selection_models = [self.views[i].selectionModel() for i in range(lsv)]
        
        # Содержимое вкладки с кнопками
        main_layout = QHBoxLayout()
        main_layout.addWidget(self.inner_tab_widget)
        self.tab1.setLayout(main_layout)
        
        inner_layouts = [QHBoxLayout() for i in range(lsv)]
        
        for i in range(len(inner_layouts)):
            inner_layouts[i].addWidget(self.views[i])
            self.tabs[i].setLayout(inner_layouts[i])
        
        if dbauth.manager.text() == "superuser":
            self.tabs.append(QWidget())
            self.inner_tab_widget.addTab(self.tabs[-1], self.tr('Outlets'))
            self.models.append(PointsEditor(self))
            self.views.append(QTableView())
            self.views[-1].setModel(self.models[-1])
            self.selection_models.append(self.views[-1].selectionModel())
            
            # Содержимое вкладки с точками
            inner_layouts.append(QHBoxLayout())
            inner_layouts[-1].addWidget(self.views[-1])
            self.tabs[-1].setLayout(inner_layouts[-1])

        self.setWindowTitle("baguette")
        # --------------------Конец self.tab1-----------------------------------
        # --------------Компоновка на self.tab2---------------------------------
        main_layout_2 = QHBoxLayout()
        self.tab2.setLayout(main_layout_2)
        self.hsplitter = QSplitter(Qt.Horizontal)
        self.h2splitter = QSplitter(Qt.Horizontal)
        self.vsplitter = QSplitter(Qt.Vertical)
        main_layout_2.addWidget(self.hsplitter)
        self.hsplitter.addWidget(self.vsplitter)
        # ----------------------------------------------------------------------
        # -------------------------Данные клиента-------------------------------
        self.client_data = QGroupBox(self.tr('Customer data'))
        self.vsplitter.addWidget(self.client_data)
        gridlay_4 = QGridLayout(self.client_data)
        
        self.lbl_second_name = QLabel(self.tr("Last name"))
        gridlay_4.addWidget(self.lbl_second_name, 0, 0)
        self.lbl_first_name = QLabel(self.tr("First name"))
        gridlay_4.addWidget(self.lbl_first_name, 0, 1)
        self.lbl_patronymic = QLabel(self.tr("Patronymic"))
        gridlay_4.addWidget(self.lbl_patronymic, 0, 2)
        self.lbl_phone = QLabel(self.tr("Phone"))
        gridlay_4.addWidget(self.lbl_phone, 0, 3)
        
        self.combo_second_name = QComboBox()
        self.combo_second_name.setEditable(True)
        gridlay_4.addWidget(self.combo_second_name, 1, 0)
        
        self.combo_first_name = QComboBox()
        self.combo_first_name.setEditable(True)
        gridlay_4.addWidget(self.combo_first_name, 1, 1)
        
        self.combo_patronymic = QComboBox()
        self.combo_patronymic.setEditable(True)
        gridlay_4.addWidget(self.combo_patronymic, 1, 2)

        self.combo_phone = QComboBox()
        self.combo_phone.setEditable(True)
        gridlay_4.addWidget(self.combo_phone, 1, 3)
        self.combo_phone.setToolTip(self.tr("Phone number format")+": +7(000)000-00-00")
        # ------------------------------------------------------------------------
        # --------------------------Опции заказа----------------------------------
        gridlay_3 = QGridLayout()
        self.tabs2[0].setLayout(gridlay_3)
        
        self.lbl_point2 = QLabel(self.tr("Outlet"))
        gridlay_3.addWidget(self.lbl_point2, 0, 0)
        self.combo_point2 = QComboBox()
        self.combo_point2.setEditable(False)
        gridlay_3.addWidget(self.combo_point2, 1, 0)
        
        self.lbl_type2 = QLabel(self.tr("Order type"))
        gridlay_3.addWidget(self.lbl_type2, 0, 2)
        self.lineedit_type2 = QLineEdit()
        gridlay_3.addWidget(self.lineedit_type2, 1, 2)
        
        self.lbl_email = QLabel(self.tr("Customer email"))
        gridlay_3.addWidget(self.lbl_email, 0, 4)
        self.combo_email = QComboBox()
        self.combo_email.setEditable(True)
        gridlay_3.addWidget(self.combo_email, 1, 4)
        
        self.lbl_note = QLabel(self.tr("Description"))
        gridlay_3.addWidget(self.lbl_note, 4, 0)
        self.note = QTextEdit()
        gridlay_3.addWidget(self.note, 5, 0, 1, 5)
        
        self.lbl_price = QLabel(self.tr("Price"))
        gridlay_3.addWidget(self.lbl_price, 6, 0)
        self.lineedit_price = QLineEdit()
        gridlay_3.addWidget(self.lineedit_price, 7, 0)
        
        lbl_multi = QLabel('×')
        gridlay_3.addWidget(lbl_multi, 6, 1 , 2, 1)
        
        self.lbl_quantity = QLabel(self.tr("Quantity"))
        gridlay_3.addWidget(self.lbl_quantity, 6, 2)
        self.spin_quantity = QSpinBox()
        self.spin_quantity.setRange(1, 100000)
        gridlay_3.addWidget(self.spin_quantity, 7, 2)
        self.spin_quantity.setValue(1)
        
        lbl_plus = QLabel('+')
        gridlay_3.addWidget(lbl_plus, 6, 3, 2, 1)
        
        self.lbl_design = QLabel(self.tr("Design price"))
        gridlay_3.addWidget(self.lbl_design, 6, 4)
        self.le_design = QLineEdit()
        self.le_design.setText("0")
        gridlay_3.addWidget(self.le_design, 7, 4)
        
        self.lbl_end2 = QLabel(self.tr("Date of completion"))
        gridlay_3.addWidget(self.lbl_end2, 8, 0)
        self.le_end2 = QLineEdit()
        self.le_end2.setToolTip(self.tr("Date format")+": DD.MM.YYYY")
        self.le_end2.setInputMask("00.00.0000;_")
        gridlay_3.addWidget(self.le_end2, 8, 2, 1, 2)
        self.btn_calendar_2 = QPushButton("")
        self.btn_calendar_2.setIcon(QIcon(os.path.join(py_dir, 'static/menu/calendar.png')))
        self.btn_calendar_2.setObjectName("calendar2")
        gridlay_3.addWidget(self.btn_calendar_2, 8, 4)
        # -----------------------------------------------------------------------------------
        # ------------------------Характеристики багета--------------------------------------
        self.vsplitter.addWidget(self.inner_tab_widget2)
        gridlay = QGridLayout()
        self.tabs2[1].setLayout(gridlay)
        
        self.lbl_point = QLabel(self.tr("Outlet"))
        gridlay.addWidget(self.lbl_point, 0, 0)
        self.combo_point = QComboBox()
        self.combo_point.setEditable(False)
        gridlay.addWidget(self.combo_point, 1, 0)
        
        self.lbl_type = QLabel(self.tr("Order type"))
        gridlay.addWidget(self.lbl_type, 0, 1)
        self.combo_type = QComboBox()
        self.combo_type.setEditable(False)
        gridlay.addWidget(self.combo_type, 1, 1)
        
        self.lbl_manuf = QLabel(self.tr("Manufacturer"))
        gridlay.addWidget(self.lbl_manuf, 2, 0)
        self.combo_manuf = QComboBox()
        self.combo_manuf.setEditable(True)
        gridlay.addWidget(self.combo_manuf, 3, 0)
        
        self.lbl_article = QLabel(self.tr("Vendor code"))
        gridlay.addWidget(self.lbl_article, 2, 1)
        self.combo_article = QComboBox()
        self.combo_article.setEditable(True)
        gridlay.addWidget(self.combo_article, 3, 1)
        
        self.lbl_width = QLabel(self.tr("Frame width (cm)"))
        gridlay.addWidget(self.lbl_width, 2, 2)
        self.combo_width = QComboBox()
        self.combo_width.setEditable(True)
        gridlay.addWidget(self.combo_width, 3, 2)
        
        self.lbl_length = QLabel(self.tr("Baguette Length (cm)"))
        gridlay.addWidget(self.lbl_length, 4, 0)
        self.line_edit_length = QLineEdit()
        gridlay.addWidget(self.line_edit_length, 5, 0)
        
        self.lbl_height = QLabel(self.tr("Baguette Width (cm)"))
        gridlay.addWidget(self.lbl_height, 4, 1)
        self.line_edit_height = QLineEdit()
        gridlay.addWidget(self.line_edit_height, 5, 1)
        
        self.lbl_end = QLabel(self.tr("Date of completion"))
        gridlay.addWidget(self.lbl_end, 6, 0)
        self.le_end = QLineEdit()
        self.le_end.setToolTip(self.tr("Date format")+": DD.MM.YYYY")
        self.le_end.setInputMask("00.00.0000;_")
        gridlay.addWidget(self.le_end, 6, 1)
        self.btn_calendar = QPushButton("")
        self.btn_calendar.setIcon(QIcon(os.path.join(py_dir, 'static/menu/calendar.png')))
        self.btn_calendar.setObjectName("calendar")
        gridlay.addWidget(self.btn_calendar, 6, 2)
        # ------------------------Подбор багета------------------------------
        self.selection = QGroupBox(self.tr('Baguette Selection'))
        self.vsplitter.addWidget(self.selection)
        inner_layout_22 = QGridLayout()
        self.selection.setLayout(inner_layout_22)
        
        self.canvas_label = QLabel("")
        self.canvas_label.setObjectName("canvas")
        self.canvas_label.setAlignment(Qt.AlignCenter)
        inner_layout_22.addWidget(self.canvas_label, 0, 0)
        
        self.image_label = QLabel("")
        self.image_label.setObjectName("bimage")
        self.image_label.setAlignment(Qt.AlignCenter)
        inner_layout_22.addWidget(self.image_label, 0, 0)
        
        self.vsplitter.setStretchFactor(0, 0)
        self.vsplitter.setStretchFactor(1, 1)
        self.vsplitter.setStretchFactor(2, 3)
        # --------------------------------------------------------------------------
        # --------------------------Паспарту----------------------------------------
        self.passe_options = QGroupBox(self.tr('Passe-partout'))
        gridlay_2 = QGridLayout(self.passe_options)
        gridlay_2.setColumnMinimumWidth(0, 10)
        gridlay_2.setColumnMinimumWidth(1, 10)
        gridlay_2.setColumnMinimumWidth(2, 10)
        inner_layout_20 = QGridLayout()
        self.tabs2[2].setLayout(inner_layout_20)
        inner_layout_20.addWidget(self.h2splitter)
        self.h2splitter.addWidget(self.passe_options)
        
        self.lbl_passe_size = QLabel(self.tr("Dimensions (cm)"))
        self.lbl_passe_size.setAlignment(Qt.AlignCenter)
        gridlay_2.addWidget(self.lbl_passe_size, 1, 1)
        
        self.passe_left = QLineEdit()
        self.passe_left.setObjectName("passe_left")
        self.passe_left.setStyleSheet("color: #888;")
        gridlay_2.addWidget(self.passe_left, 1, 0)
        self.passe_left.setText("←")
        
        self.passe_right = QLineEdit()
        self.passe_right.setObjectName("passe_right")
        self.passe_right.setStyleSheet("color: #888;")
        gridlay_2.addWidget(self.passe_right, 1, 2)
        self.passe_right.setText("→")
        
        self.passe_top = QLineEdit()
        self.passe_top.setObjectName("passe_top")
        self.passe_top.setStyleSheet("color: #888;")
        gridlay_2.addWidget(self.passe_top, 0, 1)
        self.passe_top.setText("↑")
        
        self.passe_bottom = QLineEdit()
        self.passe_bottom.setObjectName("passe_bottom")
        self.passe_bottom.setStyleSheet("color: #888;")
        gridlay_2.addWidget(self.passe_bottom, 2, 1)
        self.passe_bottom.setText("↓")
        
        self.combo_passe = QComboBox()
        self.combo_passe.setEnabled(False)
        gridlay_2.addWidget(self.combo_passe, 3, 0, 1, 3)

        # --------------------------------------------------------------------------
        # ---------------------Дополнительные опции---------------------------------
        self.extra_options = QGroupBox()
        gridlay_3 = QGridLayout(self.extra_options)
        self.lbl_descr = QLabel(self.tr('Description'))
        gridlay_3.addWidget(self.lbl_descr, 0, 0)
        self.note2 = QTextEdit()
        gridlay_3.addWidget(self.note2, 1, 0)
        self.h2splitter.addWidget(self.extra_options)

        self.lbl_glass = QLabel(self.tr('Glass'))
        gridlay_3.addWidget(self.lbl_glass, 3, 0)
        self.combo_glass = QComboBox()
        self.combo_glass.setEnabled(False)
        gridlay_3.addWidget(self.combo_glass, 4, 0)
        self.h2splitter.setStretchFactor(0, 0)
        self.h2splitter.setStretchFactor(1, 1)
        # -------------------------------------------------------------------------
        # -------------------Валидаторы--------------------------------------------
        validate_fields = [(self.combo_width, 1), (self.line_edit_length, 1), (self.line_edit_height, 1),
                           (self.passe_left, 1), (self.passe_right, 1), (self.passe_bottom, 1),
                           (self.passe_top, 1), (self.le_design, 2), (self.lineedit_price, 2)]
        for i, j in validate_fields:
            i.setValidator(QDoubleValidator(0.0, 1000000.0, j, i))
        
        phone_validator = QRegExpValidator(QRegExp("\+7\(\d{3,5}\)\d{2,3}-\d{2}-\d{2}"), self.combo_phone)
        self.combo_phone.setValidator(phone_validator)
        # --------------------------------------------------------------------------
        # -------------------Окошко предпросмотра-----------------------------------
        self.textedit = QWebEngineView()
        self.hsplitter.addWidget(self.textedit)
        self.textedit.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        # --------------------------------------------------------------------------
        # ----------------------Сигналы--------------------------------------------
        self.btn_submit.clicked.connect(self.submit)
        self.btn_revert.clicked.connect(self.revert)
        self.btn_insert_row.clicked.connect(self.insert_row)
        self.btn_delete_row.clicked.connect(self.delete_row)
        self.btn_erase_row.clicked.connect(self.erase_row)
        self.btn_calculation.clicked.connect(self.calculation)
        self.inner_tab_widget.currentChanged[int].connect(self.disable_buttons)
        self.btn_csv_import.clicked.connect(self.csv_import)
        self.btn_csv_export.clicked.connect(self.csv_export)
        
        self.combo_type.activated.connect(lambda: self.related_events(0))
        self.combo_width.activated.connect(lambda: self.related_events(1))
        self.combo_article.activated.connect(lambda: self.related_events(2))
        self.combo_second_name.activated.connect(lambda: self.related_events(3))
        self.combo_first_name.activated.connect(lambda: self.related_events(4))
        self.combo_patronymic.activated.connect(lambda: self.related_events(5))
        self.combo_phone.activated.connect(lambda: self.related_events(6))
        self.combo_point.activated.connect(lambda: self.related_events(7))
        self.combo_email.activated.connect(lambda: self.related_events(8))
        self.combo_manuf.activated.connect(lambda: self.related_events(9))
        self.btn_checkout.clicked.connect(self.checkout)
        self.btn_print.clicked.connect(self.printer)
        self.btn_search.clicked.connect(self.search)
        self.btn_sum.clicked.connect(self.summary)
        self.form.toggled.connect(self.select_form)
        self.btn_load_image.clicked.connect(self.load_img)
        self.btn_calendar.clicked.connect(lambda: self.show_calendar(0))
        self.btn_calendar_2.clicked.connect(lambda: self.show_calendar(1))
        self.vsplitter.splitterMoved.connect(self.move_splitter)
        self.btn_add_filter.clicked.connect(self.add_db_filter)
        self.btn_restore.clicked.connect(self.restore)
        self.btn_select.clicked.connect(self.select)
        for i in self.selection_models:
            i.selectionChanged.connect(self.sel_changed)
        # ---------------------------------------------------------------------
        self._filter = Filter()
        passes = [self.passe_left, self.passe_right, self.passe_top, self.passe_bottom]
        for i in passes:
            i.installEventFilter(self._filter)
        for i in self.views:
            i.installEventFilter(self._filter)
        
        try:
            settings = QSettings(os.path.join(py_dir, "baguette.ini"), QSettings.IniFormat)
            self.hsplitter.restoreState(settings.value("hsplitter"))
            self.h2splitter.restoreState(settings.value("h2splitter"))
            self.vsplitter.restoreState(settings.value("vsplitter"))
            self.restoreState(settings.value("windowstate"))
            self.restoreGeometry(settings.value("geo"))
        except TypeError:
            self.resize(1020, 620)

    def select(self):
        dic = self.model_definition()
        sm = dic["sm"]
        val1 = sm.model().index(sm.currentIndex().row(), 1).data()
        if sm == self.selection_models[0]:
            val2 = sm.model().index(sm.currentIndex().row(), 2).data()
            self.combo_manuf.setCurrentIndex(self.combo_manuf.findText(val1))
            self.combo_article.clear()
            self.manuf_combo_fill(QSqlQuery())
            self.combo_article.setCurrentIndex(self.combo_article.findText(val2))
            self.related_events(2)
        elif sm == self.selection_models[5]:
            self.combo_second_name.setCurrentIndex(self.combo_second_name.findText(val1))
            self.related_events(3)
        self.tab_widget.setCurrentIndex(0)
        
    def sel_changed(self, selected):
        if selected:
            dic = self.model_definition()
            sm = dic["sm"]
            if not dbauth.manager.text() == "superuser" and sm == self.selection_models[6]:
                col = sm.currentIndex().column()
                if col == 0 or col == 1:
                    self.views[6].setEditTriggers(QAbstractItemView.NoEditTriggers)
                else:
                    self.views[6].setEditTriggers(QAbstractItemView.DoubleClicked)
            elif sm == self.selection_models[1]:
                pass
            else:
                self.btn_delete_row.setEnabled(True)
            if self.form.isChecked():
                n = self.selection_models[3]
            else:
                n = self.selection_models[2]
            if sm == n:
                self.btn_restore.setEnabled(True)
            if sm == mw.selection_models[0] and mw.form.isChecked() or sm == mw.selection_models[5]:
                self.btn_select.setEnabled(True)

    def cache_hidden(self):
        if not self.filters == []:
            for i in self.filters:
                if i.arr[0].isHidden():
                    for j in i.arr:
                        j.show()
                    return True
        return False

    def add_db_filter(self):
        if not self.cache_hidden():
            dbf = DbFilter(self.gridonframe.rowCount())
            dbf.add_filter()
            self.filters.append(dbf)
            self.db_filters_fill(self.inner_tab_widget.currentIndex())
        
    def summary(self):
        dic = self.model_definition()
        m = dic["m"]
        sm = dic["sm"]
        if sm == self.selection_models[2] or sm == self.selection_models[3]:
            if not sm.currentIndex().column() == -1:
                data = [i.data() for i in sm.selectedIndexes()]
            else:
                data = [sm.model().index(i, m.columnCount()-1).data() for i in range(0, m.rowCount())]
            try:
                summary = reduce(lambda x, y: x + y, data)
            # Если выделены ячейки с разным типом данных, складываем строки
            except TypeError:
                for i, item in enumerate(data):
                    if not str(type(item)) == "<class 'str'>":
                        data[i] = str(data[i])
                summary = reduce(lambda x, y: x + y, data)
            QMessageBox.information(self, self.tr("Notification"), self.tr("The sum is")+" {0}".format(summary))
        else:
            pass
        
    def move_splitter(self):
        px.load_canvas()

    def show_calendar(self, b):
        if b == 0:
            button = self.btn_calendar
        else:
            button = self.btn_calendar_2
        try:
            cldr = dbauth.cldr
        # Если класс dbAuth не инициализировался и потому dbauth.cldr не определен
        except AttributeError:
            cldr = mw.cldr
        cldr.init_ui(button.x(), button.y())
        cldr.show()
        
    def select_form(self, state):
        if sys.platform.startswith('linux'):
            delimiter = "/"
        else:
            delimiter = "\\"
        dic = {True: ["baguette", self.tr("Baguette"), "static"+delimiter+"baguette_blank.html"],
               False: ["general", self.tr("General form"), "static"+delimiter+"blank.html"]}
        self.form.setObjectName(dic[state][0])
        self.form.setToolTip(dic[state][1])
        path = os.path.join(py_dir, dic[state][2])
        if state:
            self.inner_tab_widget2.removeTab(0)
            self.selection.show()
            self.inner_tab_widget2.insertTab(0, self.tabs2[1], self.tr("Baguette Features"))
            self.inner_tab_widget2.insertTab(1, self.tabs2[2], self.tr("Additional options"))
        else:
            self.inner_tab_widget2.insertTab(0, self.tabs2[0], self.tr("Order Options"))
            self.selection.hide()
            self.inner_tab_widget2.removeTab(1)
            self.inner_tab_widget2.removeTab(1)
            self.inner_tab_widget2.removeTab(1)
        self.textedit.load(QUrl.fromLocalFile(os.path.join(cur_dir, path)))
 
    def closeEvent(self, event):
        settings = QSettings(os.path.join(py_dir, "baguette.ini"), QSettings.IniFormat)
        arr = [("hsplitter", self.hsplitter.saveState()), ("h2splitter", self.h2splitter.saveState()),
               ("vsplitter", self.vsplitter.saveState()), ("windowstate", self.saveState()),
               ("geo", self.saveGeometry())]
        for i, j in arr:
            settings.setValue(i, j)
        QMainWindow.closeEvent(self, event)

    def discard_auto_auth(self):
        with shelve.open(jdbauth) as s:
            s["auto"] = 0
        QMessageBox.information(self, self.tr("Notification"), self.tr("Auto connect canceled!"))
        self.act_discard_auto_auth.setEnabled(False)
        
    def comma_change(self):
        s = shelve.open(jdbauth)
        if self.act_comma_change.isChecked():
            s["comma"] = 1
        else:
            s["comma"] = 0
        s.close()
        
    def manuf_combo_fill(self, query):
        while query.next():
            self.combo_manuf.addItem(query.value(0))
        query.prepare("SELECT article, frame_width FROM stock WHERE manufacturer = ?")
        query.addBindValue(self.combo_manuf.currentText())
        query.exec_()
        while query.next():
            self.combo_article.addItem(query.value(0))
            self.combo_width.addItem(str(query.value(1)))
        
    def combo_fill(self):
        combos = [self.combo_manuf, self.combo_article, self.combo_width, self.combo_type, self.combo_point,
                  self.combo_passe, self.combo_glass]
        for c in combos:
            c.clear()
        query = QSqlQuery()
        query.exec_("SELECT DISTINCT manufacturer FROM stock")
        self.manuf_combo_fill(query)
        query.exec_("SELECT type FROM types")
        while query.next():
            self.combo_type.addItem(query.value(0))
        query.exec_("SELECT second_name, first_name, patronymic, phone, email FROM customers")
        while query.next():
            self.combo_second_name.addItem(query.value(0))
            self.combo_first_name.addItem(query.value(1))
            self.combo_patronymic.addItem(query.value(2))
            self.combo_phone.addItem(query.value(3))
            self.combo_email.addItem(query.value(4))
        query.exec_("SELECT point FROM points")
        while query.next():
            self.combo_point.addItem(query.value(0))
            self.combo_point2.addItem(query.value(0))
        try:
            with shelve.open(jdbauth) as s:
                index = s["point"]
            self.combo_point.setCurrentIndex(index)
            self.combo_point2.setCurrentIndex(index)
        # Если не определена замена точек на запятые при экспорте
        except Exception as ex:
            template = "combo_fill: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            pass
        try:
            with shelve.open(jdbauth) as s:
                if s["comma"] == 1:
                    self.act_comma_change.setChecked(True)
        # Если не определена замена точек на запятые при экспорте
        except KeyError:
            pass
        query.exec_("SELECT type FROM passe_partout")
        while query.next():
            self.combo_passe.addItem(query.value(0))
        query.exec_("SELECT type FROM glass")
        while query.next():
            self.combo_glass.addItem(query.value(0))
        self.le_line_manager.setText(dbauth.manager.text())
        self.related_events(2)
        self.select_form(False)
        end = str(datetime.now())[:-7]
        e = end.split(" ")
        e_arr = e[0].split("-")
        end_rus = e_arr[2]+"."+e_arr[1]+"."+e_arr[0]
        self.le_end.setText(end_rus)
        self.le_end2.setText(end_rus)
        self.related_events(0)
    
    def printer(self):
        printer = QPrinter()
        dialogue = QPrintDialog(printer)
        if dialogue.exec_() == QDialog.Accepted:
            self.textedit.page().printToPdf(printer.outputFileName())
    
    def delete_confirmation(self, value):
        ret = QMessageBox.warning(
            self, self.tr("Delete confirmation"),
            self.tr('Are you sure you want to delete the position')+' "' + str(value) +
            '"?\n'+self.tr('Restore will be impossible!'), QMessageBox.Yes | QMessageBox.Default | QMessageBox.No |
                                                    QMessageBox.Cancel | QMessageBox.Escape)
        if ret == QMessageBox.Yes:
            query = QSqlQuery()
            if self.inner_tab_widget.currentIndex() == 0:
                query.prepare("DELETE FROM stock WHERE id = ?")
            elif self.inner_tab_widget.currentIndex() == 2:
                query.prepare("DELETE FROM new_order2 WHERE id = ?")
            elif self.inner_tab_widget.currentIndex() == 3:
                query.prepare("DELETE FROM new_order WHERE id = ?")
            elif self.inner_tab_widget.currentIndex() == 4:
                query.prepare("DELETE FROM types WHERE id = ?")
            elif self.inner_tab_widget.currentIndex() == 5:
                query.prepare("DELETE FROM customers WHERE id = ?")
            elif self.inner_tab_widget.currentIndex() == 6:
                query.prepare("DELETE FROM managers WHERE id = ?")
            elif self.inner_tab_widget.currentIndex() == 7:
                query.prepare("DELETE FROM points WHERE id = ?")
            query.addBindValue(value)
            query.exec_()
            self.submit()
        elif ret == QMessageBox.Cancel:
            return False
        return True
    
    def checkout(self):
        query = QSqlQuery()
        data_length = len(self.data)
        if data_length > 20:
            m = self.models[3]
            email = ""
            query.prepare("INSERT INTO new_order VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        else:
            m = self.models[2]
            email = self.data[9]
            query.prepare("INSERT INTO new_order2 VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        for i in range(0,data_length):
            query.addBindValue(self.data[i])
        query.exec_()
        self.btn_checkout.setEnabled(False)
        
        new_second_name = self.combo_second_name.currentText()
        new_first_name = self.combo_first_name.currentText()
        new_patronymic = self.combo_patronymic.currentText()
        new_phone = self.combo_phone.currentText()
        query = QSqlQuery()
        query.prepare("SELECT second_name, email FROM customers WHERE second_name = ? AND first_name = ?")
        q = [new_second_name, new_first_name]
        for i in q:
            query.addBindValue(i)
        query.exec_()
        while query.next():
            old_second_name = query.value(0)
            old_email = query.value(1)
        try:
            old_second_name
        except Exception as ex:
            template = "checkout: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            query.exec_("SELECT id FROM customers")
            while query.next():
                id = query.value(0)
            try:
                id = id + 1
            except Exception as ex:
                template = "checkout2: {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                id = 1
            ret = QMessageBox.warning(
                self, self.tr("Confirm customer addition"),
                self.tr('Are you sure you want to add a customer')+' "'+new_first_name+' '+new_second_name+'"?',
                QMessageBox.Yes | QMessageBox.Default | QMessageBox.No | QMessageBox.Cancel | QMessageBox.Escape)
            if ret == QMessageBox.Yes:
                query.prepare("INSERT INTO customers VALUES(?,?,?,?,?,?,?)")
                new_customer = [id, new_second_name, new_first_name, new_patronymic, new_phone, email,
                                str(datetime.now())[:-7]]
                for i in range(0, self.models[5].columnCount()):
                    query.addBindValue(new_customer[i])
                query.exec_()
                if self.models[5].submitAll():
                    self.models[5].database().commit()
                self.combo_second_name.clear()
                self.combo_first_name.clear()
                self.combo_patronymic.clear()
                self.combo_phone.clear()
                self.combo_email.clear()
                query.exec_("SELECT second_name, first_name, patronymic, phone, email FROM customers")
                while query.next():
                    self.combo_second_name.addItem(query.value(0))
                    self.combo_first_name.addItem(query.value(1))
                    self.combo_patronymic.addItem(query.value(2))
                    self.combo_phone.addItem(query.value(3))
                    self.combo_email.addItem(query.value(4))
        else:
            if data_length <= 20 and old_email == "":
                ret = QMessageBox.warning(
                    self, self.tr("Email Confirmation"),
                    self.tr('Add email')+' '+email+' '+self.tr('to customer table')+' "' + new_first_name+' '+
                    new_second_name+'"?',
                    QMessageBox.Yes | QMessageBox.Default | QMessageBox.No | QMessageBox.Cancel | QMessageBox.Escape)
                if ret == QMessageBox.Yes:
                    query.prepare("UPDATE customers SET email=? WHERE second_name = ? AND first_name = ?")
                    query.addBindValue(email)
                    query.addBindValue(new_second_name)
                    query.addBindValue(new_first_name)
                query.exec_()
                if self.models[5].submitAll():
                    self.models[5].database().commit()
        if m.submitAll():
            m.database().commit()
            QMessageBox.information(self, self.tr("Notification"), self.tr("Order is processed!"))
        else:
            m.database().rollback()
            QMessageBox.warning(self, "Cached Table", "The database reported an error: %s" % m.lastError().text())

    def states(self, s):
        arr = [(self.btn_insert_row, s[0]), (self.btn_delete_row, s[1]), (self.btn_sum, s[2]),
               (self.btn_restore, s[3]), (self.btn_erase_row, s[4]), (self.btn_select, s[5])]
        for a, b in arr:
            a.setEnabled(b)

    def disable_buttons(self):
        ci = self.inner_tab_widget.currentIndex()
        lst = [[True, False, False, False, True, False], [False, False, False, False, True, False],
               [False, False, True, False, True, False], [False, False, True, False, True, False],
               [True, False, False, False, True, False], [True, False, False, False, True, False],
               [], [True, False, False, False, True, False]]
        if dbauth.manager.text() == "superuser":
            lst[6] = [True, False, False, False, True, False]
        else:
            lst[6] = [False, False, False, False, False, False]
        for i, s in enumerate(lst):
            if ci == i:
                self.states(s)
        self.db_filters_fill(ci)
    
    def db_filters_fill(self, ci):
        self.combo_column.clear()
        for i in self.filters:
            i.arr[1].clear()
        try:
            matrix = dbauth.matrix
        # Если загружались по пути автоподключения
        except AttributeError:
            matrix = mw.matrix
        fn = matrix.fn[ci].keys()
        self.combo_column.addItems(list(fn))
        for i in self.filters:
            i.arr[1].addItems(list(fn))
       
    def model_definition(self):
        for i in range(0, len(self.models)):
            if self.inner_tab_widget.currentIndex() == i:
                m = self.models[i]
                sm = self.selection_models[i]
        return {"m": m, "sm": sm}
    
    def revert(self):
        dic = self.model_definition()
        m = dic["m"]
        m.revertAll()

    def erase_row(self):
        dic = self.model_definition()
        m = dic["m"]
        sm = dic["sm"]
        row = sm.currentIndex().row()
        if row == -1:
            QMessageBox.warning(self, self.tr("Erase row"), self.tr("To clear a row, select one of its fields"))
        else:
            m.removeRows(row, 1)
        
    def delete_row(self):
        dic = self.model_definition()
        sm = dic["sm"]
        value = sm.model().index(sm.currentIndex().row(), 0).data()
        try:
            if sm == self.selection_models[6] and value == 1:
                QMessageBox.warning(self, self.tr("Deleting user"), self.tr("Superuser cannot be deleted"))
            else:    
                self.delete_confirmation(value)
        except Exception as ex:
            template = "delete_row: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            self.delete_confirmation(value)
        
    def insert_row(self):
        dic = self.model_definition()
        m = dic["m"]
        m.insertRows(0, 1)

    def submit(self):
        dic = self.model_definition()
        m = dic["m"]
        m.database().transaction()
        if m.submitAll():
            m.database().commit()
        else:
            m.database().rollback()
            QMessageBox.warning(self, self.tr("Database message"),
                                "The database reported an error: %s" % m.lastError().text())
        try:
            if m in [self.models[0], self.models[4], self.models[5], self.models[7]. self.models[8]]:
                self.combo_fill()
        except Exception as ex:
            template = "submit0: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
        try:
            if m == self.models[9]:
                self.combo_fill()
        except Exception as ex:
            template = "submit: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            pass
        
    def start_rus(self, start):
        s = start.split(" ")
        s_arr = s[0].split("-")
        start_rus = s_arr[2]+"."+s_arr[1]+"."+s_arr[0]+" "+s[1]
        return start_rus

    def py_to_js(self):
        c = QWebChannel(self)
        c.registerObject("AddToJs", self.obj)
        self.textedit.page().setWebChannel(c)
        self.textedit.page().runJavaScript("f();")
        
    def create_detail(self, baguette_frame, glass, knurl, underframe, back, mounting, passe_partout, lacquer, fixture,
                      fixture_mirror):
        header = '<th colspan="2" align="left" id="detail_header">'+self.tr('Invoice detail')+'</th>'
        if baguette_frame == 0:
            s_baguette_frame = ""
        else:
            s_baguette_frame = "<tr><td>"+self.tr("Baguette frame")+":</td><td>" + str(baguette_frame) + "</td></tr>"
        if glass == 0:
            s_glass = ""
        else:
            s_glass = "<tr><td>"+self.tr("Glass")+":</td><td>" + str(glass) + "</td></tr>"
        if knurl == 0:
            s_knurl = ""
        else:
            s_knurl = "<tr><td>"+self.tr("Foam board knurling")+":</td><td>" + str(knurl)+"</td></tr>"
        if underframe == 0:
            s_underframe = ""
        else:
            s_underframe = "<tr><td>"+self.tr("Stretcher")+":</td><td>" + str(underframe)+"</td></tr>"
        if back == 0:
            s_back = ""
        else:
            s_back = "<tr><td>"+self.tr("Backcloth")+":</td><td>" + str(back) + "</td></tr>"
        if mounting == 0:
            s_mounting = ""
        else:
            s_mounting = "<tr><td>"+self.tr("Mounting")+":</td><td>" + str(mounting)+"</td></tr>"
        if passe_partout == 0:
            s_passe_partout = ""
        else:
            s_passe_partout = "<tr><td>"+self.tr("Passe-partout")+":</td><td>" + str(passe_partout) + "</td></tr>"
        if lacquer == 0:
            s_lacquer = ""
        else: s_lacquer = "<tr><td>"+self.tr("Varnishing")+":</td><td>" + str(lacquer)+"</td></tr>"
        if fixture == 0:
            s_fixture = ""
        else:
            s_fixture = "<tr><td>"+self.tr("Fastener")+":</td><td>" + str(fixture)+"</td></tr>"
        if fixture_mirror == 0:
            s_fixture_mirror = ""
        else:
            s_fixture_mirror = "<tr><td>"+self.tr("Mirror fasteners")+":</td><td>"+str(fixture_mirror)+"</td></tr>"
        
        detail = header + s_baguette_frame + s_glass + s_knurl + s_underframe + s_back + s_mounting + s_passe_partout +\
                 s_lacquer + s_fixture + s_fixture_mirror
        return detail

    def define_id(self, table):
        query = QSqlQuery()
        if table == 0:
            query.exec_("SELECT id FROM new_order")
        else:
            query.exec_("SELECT id FROM new_order2")
        while query.next():
            id = query.value(0)
        try:
            id = id + 1
        except Exception as ex:
            template = "define_id: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            id = 1
        return id

    def enable_buttons(self):
        self.btn_checkout.setEnabled(True)
        self.btn_print.setEnabled(True)
        self.act_print.setEnabled(True)
            
    def calculation(self):
        query = QSqlQuery()
        manager = self.le_line_manager.text()
        second_name = self.combo_second_name.currentText()
        first_name = self.combo_first_name.currentText()
        patronymic = self.combo_patronymic.currentText()
        phone = self.combo_phone.currentText()
        if self.form.isChecked():
            order_type = self.combo_type.currentText()
            point = self.combo_point.currentText()
            end = self.le_end.text()
            note = self.note2.toPlainText()
        else:
            order_type = self.lineedit_type2.text()
            point = self.combo_point2.currentText()
            end = self.le_end2.text()
            email = self.combo_email.currentText()
            note = self.note.toPlainText()
        fields = [first_name, phone, end]
        e_arr = end.split(".")
        end_eng = e_arr[2]+"-"+e_arr[1]+"-"+e_arr[0]
        for i in fields:
            if i == "":
                QMessageBox.warning(self, self.tr("Error"), self.tr("Not all fields are filled"))
                return False
        if self.form.isChecked():
            try:
                length = 0.01*float(self.line_edit_length.text())
                height = 0.01*float(self.line_edit_height.text())
                query.prepare("SELECT passe FROM types WHERE type = ?")
                query.addBindValue(order_type)
                query.exec_()
                while query.next():
                    passe = query.value(0)
                if passe and not self.combo_passe.currentText() == "Нет":
                    length = length + 0.01*(float(self.passe_left.text()) + float(self.passe_right.text()))
                    height = height + 0.01*(float(self.passe_top.text()) + float(self.passe_bottom.text()))
            except ValueError as ex:
                template = "calculation: {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                QMessageBox.warning(self, self.tr("Error"), self.tr("Not all fields are filled"))
            else:
                width = 0.01*float(self.combo_width.currentText())
                article = self.combo_article.currentText()
                p = 2*(length + height) + 8*width
                s = length*height
                
                query.prepare("SELECT price FROM stock WHERE article = ?")
                query.addBindValue(article)
                query.exec_()
                while query.next():
                    price = query.value(0)
                id = self.define_id(0)
                size = str(round(100*length, 2))+'×'+str(round(100*height, 2))+' '+self.tr('cmxcm')
                perimeter = str(round(p, 2))+' '+self.tr('m')
                area = str(round(s, 4))+' '+self.tr('m2')
                
                passe_type = self.combo_passe.currentText()
                glass_type = self.combo_glass.currentText()
                obj = Detail(order_type, length, height, width, p, s, price, passe_type, glass_type)
                baguette_frame = round(obj.delta_fnc(obj.baguette_frame, 1.25*obj.p*obj.price), 2)
                glass = round(obj.delta_fnc(obj.glass, obj.c_glass*obj.s), 2)
                knurl = round(obj.delta_fnc(obj.knurl, obj.c_knurl*obj.s), 2)
                underframe = round(obj.delta_fnc(obj.underframe, 2*obj.length*obj.height*obj.c_underframe), 2)
                back = round(obj.delta_fnc(obj.back, obj.c_back*obj.s), 2)
                mounting = obj.mounting_fnc()
                passe_partout = round(obj.delta_fnc(obj.passe_partout, obj.c_pass*obj.s), 2)
                lacquer = round(obj.delta_fnc(obj.lacquer, obj.c_lacquer*obj.s), 2)
                fixture = obj.delta_fnc(obj.fixture, obj.c_fixture)
                fixture_mirror = obj.delta_fnc(obj.fixture_mirror, obj.c_fixture_mirror)
                if fixture == 0:
                    f = fixture_mirror
                else:
                    f = fixture
                start = str(datetime.now())[:-7]
                total = round(baguette_frame + glass + knurl + underframe + back + mounting + passe_partout + lacquer +
                              f, 2)
                
                self.data = [id, point, manager, start, end_eng, second_name, first_name, patronymic, phone, order_type,
                             note, article, price, ""+str(round(100*length,2))+"×"+str(round(100*height,2))+"", p, s,
                             baguette_frame, glass, knurl, underframe, back, mounting, passe_partout, lacquer, f, total,
                             0, 0, 0]

                detail = self.create_detail(baguette_frame, glass, knurl, underframe, back, mounting, passe_partout,
                                            lacquer, fixture, fixture_mirror)
                s_total = str(total)+" р"
                self.enable_buttons()
                image = '<img src="'+px.draw_image()+'">'
                start_rus = self.start_rus(start)
                self.obj = AddToJs(point+str(id), manager, start_rus, end, second_name, first_name, patronymic, s_total,
                                   phone, order_type, note, image, article, price, size, perimeter, area, detail,
                                   email="", quantity=0, design=0)
                self.py_to_js()
        else:
            try:
                price = int(self.lineedit_price.text())
                quantity = int(self.spin_quantity.text())
                design = int(self.le_design.text())
            except ValueError as ex:
                template = "calculation2: {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                QMessageBox.warning(self, self.tr("Error"), self.tr("Not all fields are filled"))
            else:
                id = self.define_id(1)
                start = str(datetime.now())[:-7]
                start_rus = self.start_rus(start)
                total = price*quantity+design
                s_total = str(total)+" р"
                self.data = [id, point, manager, start, end_eng, second_name, first_name, patronymic, phone, email,
                             order_type, note, price, quantity, design, total, 0, 0, 0]
                self.enable_buttons()
                image, article, size, perimeter, area, detail = "", "", "", "", "", ""
                self.obj = AddToJs(point+str(id), manager, start_rus, end, second_name, first_name, patronymic,
                                   s_total, phone, order_type, note, image, article, price, size, perimeter, area,
                                   detail, email, quantity, design)
                self.py_to_js()

    def end_rus(self, end):
        e_arr = end.split("-")
        return e_arr[2] + "." + e_arr[1] + "." + e_arr[0]
    
    def restore(self):
        dic = self.model_definition()
        sm = dic["sm"]
        m = dic["m"]
        value = sm.model().index(sm.currentIndex().row(), 0).data()
        query = QSqlQuery()
        arr = []
        if sm == self.selection_models[2]:
            query.prepare("SELECT * FROM new_order2 WHERE id = ?")
            query.addBindValue(value)
            query.exec_()
            while query.next():
                for i in range(0,m.columnCount()):
                    arr.append(query.value(i))
            image, article, size, perimeter, area, detail = "", "", "", "", "", ""
            self.obj = AddToJs(arr[1]+str(arr[0]), arr[2], self.start_rus(arr[3]), self.end_rus(arr[4]), arr[5], arr[6],
                               arr[7], str(arr[15])+" р", arr[8], arr[10], arr[11], image, article, arr[12], size,
                               perimeter, area, detail, arr[9], arr[13], arr[14])
        elif sm == self.selection_models[3]:
            query.prepare("SELECT * FROM new_order WHERE id = ?")
            query.addBindValue(value)
            query.exec_()
            while query.next():
                for i in range(0,m.columnCount()):
                    arr.append(query.value(i))
            image = '<img src="'+px.draw_image()+'">'
            detail = self.create_detail(arr[16], arr[17], arr[18], arr[19], arr[20], arr[21], arr[22], arr[23],
                                        arr[24], fixture_mirror="")
            self.obj = AddToJs(arr[1]+str(arr[0]), arr[2], self.start_rus(arr[3]), self.end_rus(arr[4]), arr[5], arr[6],
                               arr[7], str(arr[25])+" р", arr[8], arr[10], arr[9], image, arr[11], arr[12], arr[13],
                               round(arr[14], 2), round(arr[15],2), detail, email="", quantity=0, design=0)
        self.tab_widget.setCurrentIndex(0)
        self.btn_print.setEnabled(True)
        self.act_print.setEnabled(True)
        self.py_to_js()

    def related_events(self, v):
        if v == 0:
            order_type = self.combo_type.currentText()
            query = QSqlQuery()
            query.prepare("SELECT glass, passe FROM types WHERE type = ?")
            query.addBindValue(order_type)
            query.exec_()
            while query.next():
                glass = query.value(0)
                passe = query.value(1)
            passes = [self.passe_left, self.passe_right, self.passe_top, self.passe_bottom]
            if passe:
                self.combo_passe.setEnabled(True)
                for i in passes:
                    i.setReadOnly(False)
            if not passe:
                self.combo_passe.setEnabled(False)
                for i in passes:
                    i.clear()
                    i.setReadOnly(True)
            self.combo_glass.setEnabled(glass)
            
        elif v == 1:
            index = self.combo_width.currentIndex()
            self.combo_article.setCurrentIndex(index)
            px = Pix()
            px.load_baguette()
        elif v == 2:
            index = self.combo_article.currentIndex()
            self.combo_width.setCurrentIndex(index)
            px = Pix()
            px.load_baguette()
        elif v == 3:
            index = self.combo_second_name.currentIndex()
            arr = [self.combo_first_name, self.combo_patronymic, self.combo_phone, self.combo_email]
            for i in arr:
                i.setCurrentIndex(index)
        elif v == 4:
            index = self.combo_first_name.currentIndex()
            arr = [self.combo_second_name, self.combo_patronymic, self.combo_phone, self.combo_email]
            for i in arr:
                i.setCurrentIndex(index)
        elif v == 5:
            index = self.combo_patronymic.currentIndex()
            arr = [self.combo_first_name, self.combo_second_name, self.combo_phone, self.combo_email]
            for i in arr:
                i.setCurrentIndex(index)
        elif v == 6:
            index = self.combo_phone.currentIndex()
            arr = [self.combo_first_name, self.combo_second_name, self.combo_patronymic, self.combo_email]
            for i in arr:
                i.setCurrentIndex(index)
        elif v == 7:
            index = self.combo_point.currentIndex()
            with shelve.open(jdbauth) as sh:
                sh["point"] = index
        elif v == 8:
            index = self.combo_email.currentIndex()
            arr = [self.combo_first_name, self.combo_second_name, self.combo_phone, self.combo_patronymic]
            for i in arr:
                i.setCurrentIndex(index)
        elif v == 9:
            self.combo_article.clear()
            self.combo_width.clear()
            self.manuf_combo_fill(QSqlQuery())
            px = Pix()
            px.load_baguette()
   
    def csv_import(self):
        file_name, _ = QFileDialog.getOpenFileName(self, self.tr("Select a file"), os.getcwd(),
                                                   self.tr("CSV data files")+" (* .csv)")
        if file_name != "":
            dic = self.model_definition()
            m = dic["m"]
            query = QSqlQuery()
            with open(file_name, "rt", encoding="utf8") as fileInput:
                for row in csv.reader(fileInput):
                    try:
                        if self.inner_tab_widget.currentIndex() == 0:
                            query.prepare("INSERT INTO stock VALUES(?,?,?,?,?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 1:
                            query.prepare("INSERT INTO materials_cost VALUES(?,?,?,?,?,?,?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 2:
                            query.prepare("INSERT INTO new_order2 VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 3:
                            query.prepare("INSERT INTO new_order VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 4:
                            query.prepare("INSERT INTO types VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 5:
                            query.prepare("INSERT INTO customers VALUES(?,?,?,?,?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 6:
                            query.prepare("INSERT INTO managers VALUES(?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 7:
                            query.prepare("INSERT INTO passe_partout VALUES(?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 8:
                            query.prepare("INSERT INTO glass VALUES(?,?,?)")
                        elif self.inner_tab_widget.currentIndex() == 9:
                            query.prepare("INSERT INTO points VALUES(?,?,?,?)")
                        for item in range(0, m.columnCount()):
                            query.addBindValue(row[item])
                        query.exec_()
                    except UnicodeDecodeError:
                        QMessageBox.warning(
                            self, self.tr("CSV import"),
                            self.tr("Something went wrong.")+"\n" +
                            self.tr("Maybe the CSV file has the wrong number of columns or encoding."))
                self.submit()
                
    def csv_export(self):
        file_name, _ = QFileDialog.getSaveFileName(self, self.tr('Save file'), os.getcwd(),
                                                   self.tr("CSV data files")+" (* .csv)")
        if file_name != "":
            with open(file_name, 'w', newline='', encoding="utf8") as fileInput:
                writer = csv.writer(fileInput)
                dic = self.model_definition()
                m = dic["m"]
                for row_number in range(m.rowCount()):
                    fields = []
                    for column_number in range(m.columnCount()):
                        d = m.data(m.index(row_number, column_number), Qt.DisplayRole)
                        if self.act_comma_change.isChecked():
                            if isinstance(d, float):
                                d = str(d).replace(".", ",")
                        fields.append(d)
                    writer.writerow(fields)

    def empty_cache(self, vs):
        for v in vs:
            if not v[2] == "":
                return True
        return False
                    
    def search(self):
        dic = self.model_definition()
        sm = dic["sm"]
        if not dbauth.manager.text() == "superuser" and sm == self.selection_models[6]:
            pass
        else:
            m = dic["m"]
            try:
                matrix = dbauth.matrix
            # Если загрузка идет по пути автоподключения
            except AttributeError:
                matrix = mw.matrix
            vs = [[matrix.fn[self.inner_tab_widget.currentIndex()][self.combo_column.currentText()],
                   self.combo_sign.currentText(), self.le_search.text()]]
            string = "{0}{1}'{2}'".format(vs[0][0], vs[0][1], vs[0][2])
            for i in self.filters:
                vs.append([matrix.fn[self.inner_tab_widget.currentIndex()][i.arr[1].currentText()],
                           i.arr[2].currentText(), i.arr[3].text()])
            if not self.empty_cache(vs):
                m.setFilter("")
            else:
                for i in vs[1:]:
                    if not i[2] == "":
                        string += " and {0}{1}'{2}'".format(i[0], i[1], i[2])
                m.setFilter(string)
            
    def load_img(self):
        file_name, _ = QFileDialog.getOpenFileName(
            self, self.tr('Upload image'), os.getcwd(),
            self.tr("Image files")+" (* .png * .jpg * .jpeg * .gif * .tiff * .svg)")
        if file_name != "":
            px = Pix(file_name)
            px.load_canvas()
            with shelve.open(jdbauth) as s:
                s["pix"] = file_name
            
    def keyPressEvent(self, event):
        if self.le_search.hasFocus():
            if event.key() == Qt.Key_Return:
                self.search()

    def about(self):
        QMessageBox.about(self, "О baguette",
                                "baguette ver.2.0.1\nАвтор: Eigensinn\nДата: 21.03.2019\nНа основе библиотеки PySide2")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle(QStyleFactory.create('Fusion'))
    app.setStyleSheet(open(os.path.join(py_dir, "style.qss"), "r").read())
    splash_pix = QPixmap(os.path.join(py_dir, "static/splash.png"))
    splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)
    splash.setMask(splash_pix.mask())
    progress_bar = QProgressBar(splash)
    progress_bar.setGeometry(splash.width()/10, 8.2*splash.height()/10, 8*splash.width()/10, splash.height()/10)
    splash.show()
    app.processEvents()
    progress_bar.setValue(0)
    dbauth = DbAuth()
    progress_bar.setValue(10)
    chl = ChangeLang()
    translator = QTranslator()
    app.installTranslator(translator)
    with shelve.open(jdbauth) as s:
        try:
            if translator.load(s["lang"] + '.qm', 'translations'):
                app.installTranslator(translator)
        except KeyError:
            pass
    # print(QLocale.system().name())
    connection = dbauth.shelve_read()
    progress_bar.setValue(20)
    if not connection:
        dbauth.show()
        splash.finish(dbauth)
    else:
        progress_bar.setValue(30)
        mw = Main()
        progress_bar.setValue(40)
        mw.act_discard_auto_auth.setEnabled(True)
        mw.cldr = Calendar()
        progress_bar.setValue(50)
        mw.matrix = kdata.Matrix()
        progress_bar.setValue(60)
        mw.show()
        progress_bar.setValue(70)
        mw.combo_fill()
        progress_bar.setValue(80)
        px = Pix()
        progress_bar.setValue(90)
        px.load_canvas()
        progress_bar.setValue(100)
        splash.finish(mw)
    sys.exit(app.exec_())
