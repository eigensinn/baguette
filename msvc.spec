# -*- mode: python -*-

from PyInstaller.utils.hooks import collect_data_files


block_cipher = None

datas = collect_data_files('shiboken2', include_py_files=True, subdir='support')
datas += collect_data_files('PySide2', include_py_files=True, subdir='support')
datas += collect_data_files('PySide2', subdir='translations\\qtwebengine_locales')

a = Analysis(['baguette.py'],
             pathex=['D:\\Programming\\workspace\\baguette',
			 'C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\Common7\\IDE\\Remote Debugger\\x64'],
             binaries=[],
             datas=datas,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['numpy'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.zipfiles,
		  a.binaries,
          a.datas,
          name='baguette',
          debug=False,
          strip=False,
          upx=False,
          runtime_tmpdir=None,
          console=False )